import re
from typing import Tuple
from sqlalchemy import false, true
from unidecode import unidecode
import sys
import pandas as pd
import numpy as np

sys.path.append('.')
from Util import Textos
from Util import Util
from Util import Db
from Util import Email

YO = 31735
CAMPOS_CONTROL = (f"{YO}, now(), {YO}, now()")
no_creadas = pd.DataFrame(columns=["pais","cuenta", "num_tienda", "nombre", "direccion",
                                   "estado", "ciudad", "latitud", "longitud"])
def leerExcel() -> (pd.DataFrame):
    """
        Devuelve un DataFrame con los datos de un Excel seleccionado
    """

    # filename = tkinter.filedialog.askopenfilename(title="Seleccione un archivo",
    # master = None)

    data = pd.read_excel("Formato_tienda.xlsx", engine='openpyxl',skiprows=8, dtype=str)

    
    data.drop(data.filter(regex='^Unnamed').columns, axis=1, inplace=True)
    data.drop(columns=[' '], inplace=True, errors='ignore')
    

    data.columns = ["pais","cuenta", "num_tienda", "nombre", "direccion",
                    "tipo_direccion","estado", "ciudad", "latitud", "longitud" ,
                    "abogado","cobertura","telefono","email","tipo_servicio",
                    "codigo_postal"]
    data.dropna(how='all',inplace=True)
    return data

@np.vectorize
def formatDatos(pais:str,cuenta: str,num_tienda: str,nombre: str,direccion: str, 
                tipo_direccion:str,estado: str, ciudad:str,latitud: str,
                longitud: str, abogado: str,cobertura:str, tel: str, email:str,
                tipo_servicio:str,codigo_postal:str):
    """
        Devuelve los datos en el formato adecuado
    """
    pais = pais.title().strip()
    cuenta = cuenta.title().strip()
    nombre = nombre.title().strip()
    
    tipo_direccion = tipo_direccion.title().strip()
    estado = estado.title().strip()
    ciudad = ciudad.title().strip()
    
    if pd.isna(direccion): direccion = "Sin especificar"
    else : direccion = direccion.title().strip()
    
    if pd.isna(latitud): latitud = "0.0"
    else : latitud = latitud.strip()
    
    if pd.isna(longitud): longitud = "0.0"
    else : longitud = longitud.strip()
    
    if pd.isna(tipo_servicio): tipo_servicio = "NULL"
    else : tipo_servicio = tipo_servicio.strip().upper()
    
    if pd.isna(codigo_postal): codigo_postal = "NULL"
    else : codigo_postal = codigo_postal.strip()
    
    try:
        float(latitud)
        float(longitud)
    except ValueError as _:
        print(f'''La latitud y longitud debe ser decimal: {latitud}, {longitud},
              por favor validar.''')
        exit()

    if pd.isna(num_tienda): num_tienda = "null"
    elif num_tienda.isdigit(): num_tienda = num_tienda.strip()
    else:
        print(f'El numero de tienda: {num_tienda} no es numerico, por favor validar.')
        exit()

    if pd.isna(abogado):abogado = "null"
    else:abogado = unidecode(abogado.lower().replace(" ", ""))

    if pd.isna(tel):tel = "null"
    else:
        tel = re.sub("^\+[\s*]*\d+","",tel).strip().replace(" ","")
        tel = re.search("^\d+",tel).group(0)
        
    if pd.isna(cobertura):cobertura= False

    if pd.isna(email): email = "null"
    else: email = unidecode(email.lower().replace(" ", ""))

    return (pais,cuenta,num_tienda,nombre,direccion, tipo_direccion,estado, 
            ciudad,latitud,longitud, abogado,cobertura, tel, email,tipo_servicio
            ,codigo_postal)
    

def validarCuentas(data: pd.DataFrame) -> (pd.DataFrame):
    """
        Crea una columna en el DataFrame inicial referente al id de la cuenta
    """

    df_cuentas = data[["cuenta", "id_pais"]]
    df_cuentas = df_cuentas.loc[~(df_cuentas.duplicated() |
                                    df_cuentas["cuenta"].isna())]

    if len(df_cuentas) != 0:

        df_cuentas = buscarCuentas(df_cuentas)
        data.loc[data["cuenta"].notna(), "cuenta"] = \
            Util.capitalizarDatos(data.loc[data["cuenta"].notna(), "cuenta"])
        data = pd.merge(data, df_cuentas, on=["cuenta", "id_pais"], how="left")

    else:
        data["id_cuenta"] = np.nan

    return data

def buscarCuentas(df_cuentas: pd.DataFrame) -> (pd.DataFrame):
    """
        Obtiene el id de todas las cuentas encontradas en el sistema
    """

    df_cuentas.reset_index(drop=True, inplace=True)

    pais = df_cuentas["id_pais"][0]

    cuentas = Util.listarDatos(df_cuentas["cuenta"], inicio="'", fin="',\n")
    cuentas += "'"
    consulta_busca_cuentas = \
        (f"SELECT name, id FROM custos.accounts WHERE name in ({cuentas}) "
            "AND status=1 AND active_status=1 AND indcustos=1 "
            f"AND country_config_id={pais};")

    Textos.agregaTexto(consulta_busca_cuentas + "\n")
    resultados = Db.ejecutarConsulta(consulta_busca_cuentas)

    if len(resultados) != 0:

        df_resultados = pd.DataFrame(resultados,
                                        columns=["cuenta", "id_cuenta"])
        df_resultados["cuenta"] = Util.capitalizarDatos(df_resultados["cuenta"])

        df_cuentas["cuenta"] = Util.capitalizarDatos(df_cuentas["cuenta"])

        df_cuentas = pd.merge(df_cuentas, df_resultados,
                                on=["cuenta"], how="left")

    else:
        df_cuentas["id_cuenta"] = np.nan

    df_no_cuentas = df_cuentas[["cuenta", "id_pais"]]\
                        .loc[df_cuentas["id_cuenta"].isna()]

    if len(df_no_cuentas) != 0:

        msg = "No se encontraron las siguientes cuentas:\n"
        msg += Util.listarDatos(df_no_cuentas["cuenta"], inicio="- ", fin="\n")
        print(msg)

        print("Desea buscarlas manualmente?\n"
                "1. Si\n"
                "2. No, en este caso se omitirá todo lo relacionado a estas")
        while True:
            op = input("> ")
            if op == "1":
                df_no_cuentas = df_no_cuentas.apply(buscarCuentasInd, axis=1)
                break
            elif op == "2":
                df_no_cuentas["id_cuenta"] = np.nan
                break
            else:
                print("Selección inválida")

        df_cuentas = df_cuentas.loc[df_cuentas["id_cuenta"].notna()]

        df_cuentas = pd.concat([df_cuentas, df_no_cuentas])

    return df_cuentas

def buscarCuentasInd(row: pd.Series) -> (pd.Series):
    """
        Busca una sola cuenta para seleccionarla manualmente
    """

    busqueda = str(row["cuenta"]).split(" ")[0]
    # print(busqueda)
    # if (busqueda.__contains__("´") or busqueda.__contains__("'")): 
    #     print("Entra")
    #     busqueda = re.sub("^\w+",busqueda)
    consulta_busca_cuentas_manual = \
        (f"SELECT name, id FROM custos.accounts WHERE name like '%{busqueda}%'"
            f"AND country_config_id={row['id_pais']} AND status=1 "
            "AND active_status=1 AND indcustos=1;")
    row["id_cuenta"] = selectConsultaUnica(consulta_busca_cuentas_manual,
                                            busqueda, str(row["id_pais"]))

    return row

def selectConsultaUnica(consulta: str, busqueda: str, lugar: str) -> Tuple[int, int]:
    """
        Realiza una consulta individual para seleccionar la busqueda deseada
    """

    Textos.agregaTexto(consulta)
    resultados = Db.ejecutarConsulta(consulta)

    if len(resultados) == 0:
        print(f"No se encontró {busqueda} en {lugar}")
        id_busqueda = np.nan

    elif len(resultados) == 1:
        print(f"Se encontró una única coincidencia para {busqueda} en {lugar}")
        id_busqueda = int(resultados[0][1])

    else:
        print(f"Para {busqueda}")
        for i, resul in enumerate(resultados, 1):
            print (f"{i} {str(resul)}")

        while True:
            sel = input("> (s para omitir la busqueda) ")
            if sel == "s":
                id_busqueda = np.nan
                break
            elif not sel.isdigit():
                print("Selección inválida")
            else:
                sel = int(sel)
                sel -= 1
                if sel >= len(resultados) or sel < 0:
                    print("Selección inválida")
                else:
                    id_busqueda = int(resultados[sel][1])
                    break

    return id_busqueda

def buscarPlaceName(df_num_tienda: pd.DataFrame):
    """
        Busca los store number en la base de datos
    """
    df_num_tienda["duplicado"] = df_num_tienda["nombre"].duplicated(keep=False)
    df_tiendas_dup = df_num_tienda.loc[df_num_tienda["duplicado"]]
    if (len(df_tiendas_dup)>0):
        print("Hay tiendas en el formato con el mismo nombre")
        no_creadas = pd.concat([no_creadas,df_tiendas_dup])
        print(df_tiendas_dup[["num_tienda","nombre","cuenta","direccion"]])
        df_num_tienda.drop_duplicates("nombre",keep=False,inplace=true)
    df_num_tienda.drop("duplicado",axis=1,inplace=True)

    comparar_tiendas_cuentas(df_num_tienda)
    

def comparar_tiendas_cuentas(df_num_tienda:pd.DataFrame):
    acc_id = list(set(map(str,df_num_tienda['id_cuenta'])))
    consulta_places = ("SELECT place_name,account_id FROM custos.places"
            f"\nWHERE account_id in ({','.join(acc_id)}) "
            "and status = 1 and active_status = 1;")

    Textos.agregaTexto(consulta_places)
    resultados = Db.ejecutarConsulta(consulta_places)

    if len(resultados) != 0:

        df_resultados = pd.DataFrame(resultados, columns=["nombre","id_cuenta"])
        df_resultados["nombre"] = df_resultados["nombre"].str.title()
        new_existentes = pd.merge(df_num_tienda, df_resultados,
                                on=["nombre","id_cuenta"], how="inner")
        if (len(new_existentes)>0):
            print("Existen los siguientes nombres de las tiendas para la cuenta")
            print(new_existentes[["num_tienda","nombre","cuenta"]])
            exit()



def buscarStoreNumber(df_num_tienda: pd.DataFrame):
    """
        Busca los store number en la base de datos
    """
    df_num_nan = df_num_tienda.loc[df_num_tienda["num_tienda"] == 'null']
    if (len(df_num_tienda) == len(df_num_nan)) : return
    df_num_tienda["duplicado"] = df_num_tienda["num_tienda"].\
                                                duplicated(keep=False)
    df_tiendas_dup = df_num_tienda.loc[df_num_tienda["duplicado"]]
    df_tiendas_dup = df_tiendas_dup[df_tiendas_dup["num_tienda"] != 'null']
    if (len(df_tiendas_dup)>0):
        print("Hay tiendas en el formato con el mismo número de tienda")
        print(df_tiendas_dup[["num_tienda","nombre","cuenta","direccion"]])
        exit()
    
    df_num_tienda.drop("duplicado",axis=1,inplace=True)
    
    acc_id = list(map(str,df_num_tienda["id_cuenta"]))
    
    consulta_username = \
    ("select a.store_number,b.account_id from store_details a, places b "
        f"where a.place_id = b.id and b.account_id in ({','.join(acc_id)}) "
        "and b.status = 1 and b.active_status = 1;")

    Textos.agregaTexto(consulta_username)
    resultados = Db.ejecutarConsulta(consulta_username)
    if len(resultados) != 0:
        df_resultados = pd.DataFrame(resultados, 
                columns=["num_tienda","id_cuenta"])
        df_resultados["num_tienda"] = list(map(str,df_resultados["num_tienda"]))
        new_existentes = pd.merge(df_num_tienda, df_resultados,
                                on=["num_tienda","id_cuenta"], how="inner")

        if (len(new_existentes)>0):
            print("Existen los siguientes número de las tiendas para la cuenta")
            print(new_existentes[["num_tienda","nombre","cuenta"]])
            exit()
    
def buscar_estado_ciudad(df:pd.DataFrame):
    global no_creadas
    consulta_staste = ("select id_state,state as estado,abbrev from state " 
        f"where id_country = {df['id_pais'][0]}; ")
    Textos.agregaTexto(consulta_staste)
    res_df = pd.read_sql(consulta_staste,Db.db)
    df["new_estado"] = df["estado"].str.lower().apply(Util.normalizar)
    res_df["new_estado"] = res_df["estado"].str.lower().apply(Util.normalizar)
    df = pd.merge(df,res_df.drop("estado",axis=1),'left','new_estado')
    df_not_state = df[pd.isna(df["id_state"])]
    if (len(df_not_state)==len(df)):
        df["abbrev"] = df["new_estado"]
        res_df["abbrev"] = res_df["abbrev"].str.lower().apply(Util.normalizar)
        df = pd.merge(df.drop("id_state",axis= 1),res_df.drop("estado",axis=1),'left','abbrev')
        df_not_state = df[pd.isna(df["id_state"])]    
    if (len(df_not_state)>0):
        df.loc[pd.isna(df["id_state"]), "id_state"] =  state_manual(
            df_not_state["estado"],df_not_state["id_pais"])
    df.loc[df["id_state"] == 0, "id_state"] = np.nan
    no_creadas = no_creadas.append(df[pd.isna(df["id_state"])][["pais","cuenta", "num_tienda", "nombre", "direccion",
                            "estado", "ciudad", "latitud", "longitud"]])
    if len(df[pd.isna(df["id_state"])]) > 0: 
        no_creadas["Info"] = "Para esta tienda no se encontró el estado"
    df.dropna(subset=["id_state"], inplace=True)
    stat = list(set(map(str,df["id_state"])))
    consulta_city = ("select id_city,city as ciudad, id_state " 
        f"from city where id_state in ({','.join(stat)});")
    res_df = pd.read_sql(consulta_city,Db.db)
    Textos.agregaTexto(consulta_city)
    df["new_ciudad"] = df["ciudad"].apply(Util.normalizar)
    res_df["new_ciudad"] = res_df["ciudad"].apply(Util.normalizar)
    
    df = pd.merge(df,res_df.drop('ciudad',axis=1),'left',['new_ciudad','id_state'])
    df_not_city = df[pd.isna(df["id_city"])]
    if (len(df_not_city)>0):
        df.loc[pd.isna(df["id_city"]), "id_city"] =  city_manual(
            df[pd.isna(df["id_city"])]["id_state"],df[pd.isna(df["id_city"])]["ciudad"])
    no_creadas = no_creadas.append(df[pd.isna(df["id_city"])][["pais","cuenta", "num_tienda", "nombre", "direccion",
                            "estado", "ciudad", "latitud", "longitud"]])
    if len(df[pd.isna(df["id_city"])]) > 0: 
        no_creadas["Info"] = "Para esta tienda no se encontró la ciudad es ese estado"
    df.dropna(subset=["id_city"],inplace=True)
    return df

@np.vectorize
def state_manual(valor:str,pais):
    states = valor.split()
    if len(states) > 1:
        for es in states:
            if len(es) > 3:
                state  = es
                break
    else: state= states[0]
    q = f"select id_state from state where id_country = {pais} and state like '%%{state}%%'"
    res = Db.ejecutarConsulta(q) 
    if (res):  
        return res[0][0]
    else:  
        print(f"No se encontró el estado {valor}")
        return 0
    
@np.vectorize
def city_manual(state,valor:str):
    q = f"select id_city,city from city where id_state = {state} and city like '%%{valor.split()[0]}%%'"
    res = Db.ejecutarConsulta(q)
    if (res): 
        if len(res) == 1:
            return float(res[0][0])
        else:
            print()
            for i,val in enumerate(res):
                print(f"{i+1}. {val[0]} - {val[1]} ") 
            while True:
                sel = input(f"\n\t Ingrese seleccion de la ciudad {valor} (Si no, 'n') >")
                if sel == 'n': return np.nan
                try:
                    return res[int(sel)-1][0]
                except ValueError as _:
                    print("\tValor invalido")
                    continue
    else:  
        return np.nan

def buscar_tipo_direccion(df:pd.DataFrame):
    consulta_tipo_add =\
        ("select id_address_type,address_type as tipo_direccion"
        f" from address_type where id_country = {df['id_pais'][0]};")
    if df["id_pais"][0]  == 1:
        df["tipo_direccion"] = "Commercial"

    Textos.agregaTexto(consulta_tipo_add)
    res_df = pd.read_sql(consulta_tipo_add,Db.db)
    df["tipo_direccion"] = df["tipo_direccion"].apply(Util.normalizar)
    res_df["tipo_direccion"] = res_df["tipo_direccion"].apply(Util.normalizar)
    return pd.merge(df,res_df,'inner',['tipo_direccion'])


def buscar_zone_id(df:pd.DataFrame):
    country = df["id_pais"][0]
    acc = list(set(map(str,df["id_cuenta"].to_list())))
    key = lambda _: 'Desconocido' if country not in (1,6) else 'Unknown'
    consulta_zone_id = f"""select 
        c.id as 'zone_id', a.account_id as id_cuenta
    from geographic_locations a
    inner join geographic_locations b on a.id = b.value_parent_id
    inner join geographic_locations c on b.id = c.value_parent_id
    where a.account_id in ({','.join(acc)}) 
    and c.description_value = '{key}' and 
    a.description_value = '{key}' and b.description_value  = '{key}'
    and a.parameter_id=(select id from parameters 
            where country_id={country} and table_id=166 and value_id=1)
    and b.parameter_id=(select id from parameters 
            where country_id={country} and table_id=166 and value_id=2)
    and c.parameter_id=(select id from parameters 
            where country_id={country} and table_id=166 and value_id=3);"""
    Textos.agregaTexto(consulta_zone_id)
    res_df = pd.read_sql(consulta_zone_id,Db.db)  
    
    if (len(res_df) == 0): 
        df_new_zone_id = create_zone_id(df["id_cuenta"].drop_duplicates(),key,
                                            country)
        return pd.merge(df,df_new_zone_id,'inner','id_cuenta')
    df = pd.merge(df,res_df,'inner',['id_cuenta'])   
    df["check"] = df["zone_id"].isna()
    df_no_zone = df.loc[df["check"]]
    if (len(df_no_zone) > 0):
        df_new_zone_id = create_zone_id(df_no_zone["id_cuenta"],country)
        df = pd.merge(df,df_new_zone_id,'inner','id_cuenta')
    df.drop("check",axis=1, inplace=True)

    return df
        
def create_zone_id(acc,key,countr_id):
    q_id = 'select max(id)+1 from geographic_locations;'
    max_id = Db.ejecutarConsulta(q_id)[0][0]
    Textos.agregaTexto(q_id)
    q_value = ('select id from parameters '
               f'where table_id = 166 and country_id = {countr_id}')
    res = Db.ejecutarConsulta(q_value)
    Textos.agregaTexto(q_value)
    list_df = []
    for ac in acc:
        q_insert = f'''INSERT INTO geographic_locations 
        (id,value_id,description_value,value_parent_id,account_id,parameter_id)
        VALUES 
            ({max_id},1,'{key}',0,{ac},{res[0][0]}),
            ({max_id+1},2,'{key}',{max_id},{ac},{res[1][0]}),
            ({max_id+2},3,'{key}',{max_id+1},{ac},{res[2][0]});'''
        list_df.append([max_id+2,ac])
        max_id+=3
        Textos.agregaTexto(q_insert)
    return pd.DataFrame(list_df,columns=["zone_id","id_cuenta"])
        
def buscar_tipo_lugar(country):
    q_tipo = ('select id from parameters where '
        f'table_id = 1 and country_id = {country} and value_id = 3;')
    Textos.agregaTexto(q_tipo)
    return Db.ejecutarConsulta(q_tipo)[0][0]

def insert_telefono(df:pd.DataFrame):
    df.loc[df["telefono"] != 'null' ,"phone_type_id"] = Util.phone_type(df["id_pais"][0])
    df.loc[df["telefono"] != 'null' ,"phone_country_code_id"] = Util.phone_country_code(df["id_pais"][0])
    q = "insert into place_phones (phone_type_id,phone_number,place_id,phone_code_id,status) values"
    for _,row in df.iterrows():
        if (not row["telefono"]): continue
        q += f"\n({row['phone_type_id']},{row['telefono']},{row['id']},{row['phone_country_code_id']},1),"
    q = q[:-1]+";"
    return q

def insert(df:pd.DataFrame):
    q_places = """INSERT INTO places 
    (id, type_place_id, place_name, account_id, status, created_by, created_at, 
    last_updated_by, updated_at, type, country_config_id, active_status) VALUES"""
    
    q_address = """INSERT INTO custos.place_addresses (place_id,
        id_address_type,address,id_country,id_state,id_city,zip_code,status,
        latitude,longitude) VALUES"""
    q_store_details = """INSERT INTO custos.store_details (status, store_number, 
        place_id, zone_id,lawyer_responsible_id,type_service) VALUES"""
    q_max = 'SELECT max(id)+1 from places;'
    max_id = Db.ejecutarConsulta(q_max)[0][0]
    df["id"] = [max_id+i for i in range(1,len(df)+1)]
    q_tel = insert_telefono(df)
    for _,row in df.iterrows():
        q_places+= (f"""\n({row['id']},{row['type_place_id']},"{row['nombre']}","""
        f"{row['id_cuenta']},1,{CAMPOS_CONTROL},'Store',{row['id_pais']},1),")
        
        q_address+=(f"\n({row['id']},{row['id_address_type']},"
            f""""{row['direccion']}",{row['id_pais']},{row['id_state']},"""
            f"{row['id_city']},{row['codigo_postal']},1,{row['latitud']},{row['longitud']}),")
        if (row['num_tienda'] == "null"): store_number = row['id']
        else: store_number = row['num_tienda']
        q_store_details+= \
            (f"\n(1,{store_number},{row['id']},{row['zone_id']},"
                f"{row['idaltop_abogado']},{row['id_tipo_servicio']}),")
    Textos.agregaTexto(q_places[:-1]+";\n\n")
    Textos.agregaTexto(q_address[:-1]+";\n\n")
    Textos.agregaTexto(q_store_details[:-1]+";")
    Textos.agregaTexto(q_tel)
    Textos.crearArchivo("/home/d3lt409/Documentos/MySQL/Python_creacion_places/")
    
def main():
    data = leerExcel()
    data['pais'],data['cuenta'], data['num_tienda'], data['nombre'],\
    data['direccion'],data['tipo_direccion'], data['estado'],data['ciudad'],\
    data['latitud'],data['longitud'],data['abogado'],data['cobertura'],\
    data['telefono'],data['email'],data["tipo_servicio"],data["codigo_postal"] \
        = formatDatos( data['pais'],data['cuenta'], data['num_tienda'], 
                      data['nombre'], data['direccion'],data['tipo_direccion'],
                      data['estado'],data['ciudad'], data['latitud'],
                      data['longitud'],data['abogado'], data['cobertura'],
                        data['telefono'],data['email'],data["tipo_servicio"],
                        data['codigo_postal'])
        
    data['id_pais'] = Util.setPais(data['pais'][0])
    data['id_tipo_servicio'] = Util.tipo_servicio_tienda(data['id_pais'],
                                                data['tipo_servicio'])
    df_emails = pd.DataFrame()
    df_emails['email'] = data['email'].loc[data['email'] != 'null']

    if (len(df_emails) > 0):
        df_emails["pais"] = data["id_pais"][0]
        Email.validarFormato(df_emails)
        
    df_emails['email'] = data['abogado'].loc[data['abogado'].str.contains('@')]
    if (len(df_emails) > 0):
        df_emails["pais"] = data["id_pais"][0]
        Email.validarFormato(df_emails)
        data = pd.merge(data,Email.validarExistencia(df_emails,True),'left','abogado')
    if (Email.validarExistencia(
        data.loc[data['abogado'].str.isdigit()],False)):
        data['idaltop_abogado'] = data['abogado']
    if ('idaltop_abogado' not in data):
        data['idaltop_abogado'] = 'NULL'

    data = validarCuentas(data)
    buscarPlaceName(data)
    buscarStoreNumber(data)
    data = buscar_estado_ciudad(data)

    data = buscar_zone_id(data)
    data = buscar_tipo_direccion(data)
    data["type_place_id"] = buscar_tipo_lugar(Util.setPais(data['pais'][0]))
    insert(data)
    if (len(no_creadas) > 0):
        print("No se crean algunas tiendas")
        print(no_creadas)
        no_creadas.to_excel("NO_creadas.xlsx",index=False)

if __name__ == '__main__':

    Db.iniciarConexion()

    main()

    Db.cerrarConexion()

#To do list

"""
    1. Revisar el formato:
        - Law Enforcement o Tienda (Que se pueda ambas en este archivo)
        - País
        - Store Number
        - Nombre lugar
        - Tipo de Dirección
        - Estado
        - Ciudad
        - Región
        - Abogado encargado
        - Teléfono
        - Email
        - Latitud y Longitud** Se podría igual hacer
            una función para buscarlo o con una API
    2. Format Datos
    3. Validar cuenta
    4. Validar que tienda no exista:
        Place name y en caso de existir ver la dirección
    5. Set país                     - Util
    6. Set tipo de lugar            - Util
    7. Set tipo de dirección        - Util
    8. Set estado                   - Buscar en BD o Util
    9. Set ciudad                   - Buscar en BD
    10. Set región                  - Buscar en BD
    11. Hacer el insert
    12. También generar el log (Textos.agregaTexto(consulta))
"""