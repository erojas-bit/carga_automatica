from unidecode import unidecode
import tkinter.filedialog
import validate_email
import pandas as pd
import numpy as np
import sys

sys.path.append('.')
from Util import Db 
from Util import Servicio
from Util import Textos
from Util import Util

YO = # ID Interno
CAMPOS_CONTROL = (f'{YO}, now(), {YO}, now()')

def leerExcel() -> (pd.DataFrame):
    '''
        Devuelve un DataFrame con los datos de un Excel seleccionado
    '''

    filename = tkinter.filedialog.askopenfilename(title='Seleccione un archivo')

    data = pd.read_excel(filename, skiprows=7, dtype=str)

    data.drop(data.filter(regex='^Unnamed').columns, axis=1, inplace=True)
    
    data.drop(columns=[' '], inplace=True, errors='ignore')

    data.columns = ['pais', 'nombre1', 'nombre2', 'apellido1', 'apellido2',
                    'email', 'cuenta', 'rol', 'tipo', 'tienda',
                    'state', 'phone']

    data.drop_duplicates(subset=['email', 'cuenta', 'tienda', 'state'],
                            keep='first', inplace=True)

    return data

@np.vectorize
def formatDatos(nombre1: str, nombre2: str, apellido1: str,
                apellido2: str, email: str) -> (str, str, str, str, str):
    '''
        Devuelve los datos en el formato adecuado
    '''

    nombre1 = nombre1.title().strip()
    apellido1 = apellido1.title().strip()
    email = unidecode(email.lower().replace(' ', ''))

    if str(nombre2) == 'nan':
        nombre2 = 'null'
    else:
        nombre2 = nombre2.title().strip()

    if str(apellido2) == 'nan':
        apellido2 = 'null'
    else:
        apellido2 = apellido2.title().strip()

    return (nombre1, nombre2, apellido1, apellido2, email)

def validarEmails(data: pd.DataFrame) -> (bool):
    '''
        Hace 4 validaciones para los emails:
            1. Si existen en el sistema
            2. Si tienen formato correcto
            3. Si son correos personales
            4. Si son correos yopmail
    '''

    comprobante = ''

    df_emails = pd.DataFrame(columns=['email', 'check_email', 'dominio'])

    df_emails['email'] = data['email'].drop_duplicates()

    correos = Util.listarDatos(df_emails['email'], inicio='\'', fin='\',\n')
    correos += '\''

    consulta_busca_emails = ('SELECT idaltop, email FROM custos.person '
                                f'WHERE email IN (\n{correos});')

    resultados = Db.ejecutarConsulta(consulta_busca_emails)

    per_emails = ['gmail', 'hotmail', 'yahoo', 'outlook', 'ymail']

    checkEmailVector = np.vectorize(validate_email.validate_email)

    df_emails['check_email'] = checkEmailVector(df_emails['email'])
    df_emails['dominio'] = Util.getDominio(df_emails['email'])

    bad_emails_list = df_emails['email'].loc[~(df_emails['check_email'])]
    per_emails_list = \
        df_emails['email'].loc[df_emails['dominio'].isin(per_emails)]
    yop_emails_list = df_emails['email'].loc[df_emails['dominio'].eq('yopmail')]

    rep_emails_msg = ('Los siguientes emails ya existen en el sistema, por '
                        'favor confirmar si se omiten o qué gestión tendrán:')
    bad_emails_msg = ('Los siguientes emails tienen un formato incorrecto, por '
                        'favor confirmar si se omiten o qué gestión tendrán:')
    per_emails_msg = ('Los siguientes emails son correos personales y podrían '
                        'recibir información sensible, por favor confirmar '
                        'si se omiten o qué gestión tendrán:')
    yop_emails_msg = ('Los siguientes emails tienen dominio yopmail y no se '
                        'pueden crear, por favor confirmar si '
                        'se omiten o cuáles son los correctos:')

    rep_emails_msg, comprobante = \
        setEmailMsg(rep_emails_msg, resultados, comprobante, 'r')
    bad_emails_msg, comprobante = \
        setEmailMsg(bad_emails_msg, bad_emails_list, comprobante, 'b')
    per_emails_msg, comprobante = \
        setEmailMsg(per_emails_msg, per_emails_list, comprobante, 'p')
    yop_emails_msg, comprobante = \
        setEmailMsg(yop_emails_msg, yop_emails_list, comprobante, 'y')

    check = 1
    if 'b' in comprobante:
        print(bad_emails_msg)
        check = 2
    if 'p' in comprobante:
        print(per_emails_msg)
        check = 2
    if 'r' in comprobante:
        print(rep_emails_msg)
        check = 3
    if 'y' in comprobante:
        print(yop_emails_msg)
        check = 3
    return check

def setEmailMsg(msg: str, datos: list,
                comprobante: str, check: str) -> (str, str):
    '''
        Si la lista de entrada tiene datos los enlista al final del mensaje
        y agrega el comprobante
    '''
    if len(datos) != 0:
        datos = [str(i) for i in datos]
        msg += Util.listarDatos(datos, inicio='\n- ')
        comprobante += check

    return msg, comprobante

def eliminarDuplicados(data: pd.DataFrame) -> (pd.DataFrame):
    '''
        Elimina los valores duplicados según el Manual:
            - Si es sysadmin no ponerle cuentas, tiendas, ni estados
            - Si es KAM no ponerle tiendas
            - Si es Client Corporate externo ponerle TODAS las tiendas
    '''

    # Sysadmin

    data = data.loc[~(data['id_rol'].eq(2) & data.duplicated(subset=['email']))]

    data.loc[data['id_rol'].eq(2), 'cuenta'] = np.nan
    data.loc[data['id_rol'].eq(2), 'tienda'] = np.nan
    data.loc[data['id_rol'].eq(2), 'state'] = np.nan

    # Kam

    data = data.loc[~(data['id_rol'].eq(3) &
                    data.duplicated(subset=['email', 'cuenta']))]

    data.loc[data['id_rol'].eq(3), 'tienda'] = np.nan
    data.loc[data['id_rol'].eq(3), 'state'] = np.nan

    # Lawyer

    data = data.loc[~(data['id_rol'].eq(5) & data['state'].notna() &
                    data.duplicated(subset=['email', 'cuenta']))]

    data.loc[(data['id_rol'].eq(5) & data['state'].notna()), 'tienda'] = np.nan

    # Client Corporate

    data = data.loc[~(data['id_rol'].eq(7) &
                    data['id_tipo'].isin([2, 14, 22, 18, 10, 35, 37]) &
                    data.duplicated(subset=['email', 'cuenta', 'tienda']))]
    
    data.loc[(data['id_rol'].eq(7) &
                data['id_tipo'].isin([3, 15, 23, 19, 11, 36, 38])),
                'tienda'] = 'Todas'

    # Client Store y Client Report

    data.loc[(data['id_rol'].isin([9, 20]) & data['cuenta'].isna()),
                'csr_bad'] = True

    data.loc[data['state'].notna(), 'tienda'] = np.nan

    return data

def validarCuentas(data: pd.DataFrame) -> (pd.DataFrame):
    '''
        Crea una columna en el DataFrame inicial referente al id de la cuenta
    '''

    df_cuentas = data[['cuenta', 'pais']]
    df_cuentas = df_cuentas.loc[~(df_cuentas.duplicated() |
                                    df_cuentas['cuenta'].isna())]

    if len(df_cuentas) != 0:

        df_cuentas = buscarCuentas(df_cuentas)
        
        data.loc[data['cuenta'].notna(), 'cuenta'] = \
            Util.capitalizarDatos(data.loc[data['cuenta'].notna(), 'cuenta'])
        data = pd.merge(data, df_cuentas, on=['cuenta', 'pais'], how='left')

    else:
        data['id_cuenta'] = np.nan

    return data

def buscarCuentas(df_cuentas: pd.DataFrame) -> (pd.DataFrame):
    '''
        Obtiene el id de todas las cuentas encontradas en el sistema
    '''

    df_cuentas.reset_index(drop=True, inplace=True)

    pais = df_cuentas['pais'][0]

    cuentas = Util.listarDatos(df_cuentas['cuenta'], inicio='\'', fin='\',\n')
    cuentas += '\''
    consulta_busca_cuentas = \
        (f'SELECT name, id FROM custos.accounts WHERE name IN (\n{cuentas}) '
            'AND status=1 AND active_status=1 AND indcustos=1 '
            f'AND country_config_id={pais};')

    Textos.agregaTexto(consulta_busca_cuentas + '\n')   
    resultados = Db.ejecutarConsulta(consulta_busca_cuentas)

    if len(resultados) != 0:

        df_resultados = pd.DataFrame(resultados,
                                        columns=['cuenta', 'id_cuenta'])
        df_resultados['cuenta'] = Util.capitalizarDatos(df_resultados['cuenta'])

        df_cuentas['cuenta'] = Util.capitalizarDatos(df_cuentas['cuenta'])

        df_cuentas = pd.merge(df_cuentas, df_resultados,
                                on=['cuenta'], how='left')

    else:
        df_cuentas['id_cuenta'] = np.nan

    df_no_cuentas = df_cuentas[['cuenta', 'pais']]\
                        .loc[df_cuentas['id_cuenta'].isna()]

    if len(df_no_cuentas) != 0:

        msg = 'No se encontraron las siguientes cuentas:\n'
        msg += Util.listarDatos(df_no_cuentas['cuenta'], inicio='- ', fin='\n')
        print(msg)

        print('Desea buscarlas manualmente?\n'
                '1. Si\n'
                '2. No, en este caso se omitirá todo lo relacionado a estas')
        while True:
            op = input('> ')
            if op == '1':
                df_no_cuentas = df_no_cuentas.apply(buscarCuentasInd, axis=1)
                break
            elif op == '2':
                df_no_cuentas['id_cuenta'] = np.nan
                break
            else:
                print('Selección inválida')

        df_cuentas = df_cuentas.loc[df_cuentas['id_cuenta'].notna()]

        df_cuentas = pd.concat([df_cuentas, df_no_cuentas])
        
        df_cuentas['cuenta'] = Util.capitalizarDatos(df_cuentas['cuenta'])
        
    return df_cuentas

def buscarCuentasInd(row: pd.Series) -> (pd.Series):
    '''
        Busca una sola cuenta para seleccionarla manualmente
    '''

    busqueda = str(row['cuenta']).split()[0]

    consulta_busca_cuentas_manual = \
        ('SELECT name, id FROM custos.accounts '
            f'WHERE name like \'%{busqueda}%\''
            f'AND country_config_id={row["pais"]} AND status=1 '
            'AND active_status=1 AND indcustos=1;')

    row['id_cuenta'] = selectConsultaUnica(consulta_busca_cuentas_manual,
                                            busqueda, str(row['pais']))

    return row

def validarEstados(data: pd.DataFrame) -> (pd.DataFrame):
    '''
        Crea una columna en el DataFrame inicial referente al id del estado
    '''

    df_states = \
        data[['email', 'state', 'id_cuenta', 'pais']].loc[data['state'].notna()]

    if len(df_states) != 0:

        df_states = ajustarEstados(df_states)

        df_states = buscarEstados(df_states)

        data.drop(columns=['state'], inplace=True)
        data = data.astype({'id_cuenta': str})

        data = pd.merge(data, df_states,
                        on=['email', 'id_cuenta', 'pais'], how='left')

    else:
        data['id_state'] = np.nan

    return data

def ajustarEstados(df_states: pd.DataFrame) -> (pd.DataFrame):
    '''
        Ajusta cada estado en filas diferentes
    '''

    # Separa los estados por un separador (~) pero las mantiene atadas tanto a
    # la cuenta como al usuario correspondiente

    df_states['concatenacion'] = \
        Util.concatValores(df_states['email'],
                            df_states['id_cuenta'].astype(str), separador='~')

    df_states_ind = pd.DataFrame(df_states['state'].str.split('~').tolist(),
                                    index=df_states['concatenacion']).stack()

    df_states.reset_index(drop=True, inplace=True)
    df_states_ind = df_states_ind.reset_index([0, 'concatenacion'])

    df_states_ind[['email', 'id_cuenta']] = \
        df_states_ind['concatenacion'].str.split('~', expand=True)

    df_states_ind.drop(columns=['concatenacion'], inplace=True)

    df_states_ind.columns = ['state', 'email', 'id_cuenta']

    df_states_ind['state'] = Util.capitalizarDatos(df_states_ind['state'])

    df_states_ind['state'] = \
        df_states_ind['state'].str.replace('\\s+', ' ', regex=True)

    df_states_ind['pais'] = df_states['pais'][0]

    return df_states_ind

def buscarEstados(df_states: pd.DataFrame) -> (pd.DataFrame):
    '''
        Obtiene el id de todos los estados encontradas en el sistema
    '''

    pais = df_states['pais'][0]

    states = Util.listarDatos(df_states['state'].drop_duplicates(),
                                inicio='\'', fin='\',\n')
    states += '\''
    consulta_busca_state = ('SELECT state, id_state FROM custos.state '
                            f'WHERE state IN (\n{states}) '
                            f'AND id_country={pais};')

    Textos.agregaTexto(consulta_busca_state + '\n')
    resultados = Db.ejecutarConsulta(consulta_busca_state)

    if len(resultados) != 0:

        df_resultados = pd.DataFrame(resultados,
                                        columns=['state', 'id_state'])
        df_resultados['state'] = Util.capitalizarDatos(df_resultados['state'])

        df_states = pd.merge(df_states, df_resultados, on=['state'], how='left')

    else:
        df_states['id_state'] = np.nan

    df_no_state_all = (df_states[['email', 'id_cuenta', 'state', 'pais']]
                        .loc[df_states['id_state'].isna()])

    df_no_states = df_no_state_all[['state', 'pais']].drop_duplicates()

    if len(df_no_states) != 0:

        msg = 'No se encontraron los siguientes estados:\n'
        msg += Util.listarDatos(df_no_states['state'], inicio='- ', fin='\n')
        print(msg)

        print('Desea buscarlos manualmente?\n'
                '1. Si\n'
                '2. No, en este caso se omitirá todo lo relacionado a estos')
        while True:
            op = input('> ')
            if op == '1':

                df_no_states = df_no_states.apply(buscarEstadosInd, axis=1)
                df_no_state_all = pd.merge(df_no_state_all, df_no_states,
                                            on=['state', 'pais'], how='left')
                break
            elif op == '2':
                df_no_state_all['id_state'] = np.nan
                break
            else:
                print('Selección inválida')

        df_states = df_states.loc[df_states['id_state'].notna()]

        df_states = pd.concat([df_states, df_no_state_all])

    return df_states

def buscarEstadosInd(row: pd.Series) -> (pd.Series):
    '''
        Busca un solo estado para seleccionarlo manualmente
    '''

    busqueda = str(row['state']).split()[0]

    consulta_busca_states_manual = ('SELECT state, id_state FROM custos.state '
                                    f'WHERE state like \'%{busqueda}%\' '
                                    f'AND id_country={row["pais"]};')

    row['id_state'] = selectConsultaUnica(consulta_busca_states_manual,
                                            busqueda, str(row['pais']))

    return row

# Separar las tiendas individualmente
# Contar las tiendas
# Se sale y vuelve
# Buscar las tiendas individualmente

def formatTiendas(data: pd.DataFrame) -> (pd.DataFrame):
    '''
        Separa las tiendas una por fila
    '''

    df_tiendas = data[['email', 'id_cuenta', 'tienda']]
    df_tiendas = df_tiendas.loc[df_tiendas['id_cuenta'].notna()]
    df_tiendas = df_tiendas.loc[df_tiendas['tienda'].notna()]
    
    if len(df_tiendas) != 0:

        df_tiendas_all, df_tiendas_ind = ajustarTiendas(df_tiendas)

        df_tiendas = pd.concat([df_tiendas_all, df_tiendas_ind])

        data = data.astype({'id_cuenta': str})
        data.drop(columns=['tienda'], inplace=True)

        data = pd.merge(data, df_tiendas, on=['email', 'id_cuenta'], how='left')
        
    else:
        data['all'] = False

    return data

def ajustarTiendas(df_tiendas: pd.DataFrame) -> (pd.DataFrame):
    '''
        Ajusta cada tienda en filas diferentes
    '''

    # Separa las tiendas por un separador (~) pero las mantiene atadas tanto a
    # la cuenta como al usuario correspondiente

    df_tiendas['concatenacion'] = \
        Util.concatValores(df_tiendas['email'],
                            df_tiendas['id_cuenta'].astype(str), separador='~')

    df_tiendas['tienda'] = df_tiendas['tienda'].astype(str)
    df_tiendas_ind = pd.DataFrame(df_tiendas['tienda'].str.split('~').tolist(),
                                    index=df_tiendas['concatenacion']).stack()
    df_tiendas_ind = df_tiendas_ind.reset_index([0, 'concatenacion'])

    df_tiendas_ind[['email', 'id_cuenta']] = \
        df_tiendas_ind['concatenacion'].str.split('~', expand=True)

    df_tiendas_ind.drop(columns=['concatenacion'], inplace=True)
    df_tiendas_ind.columns = ['tienda', 'email', 'id_cuenta']
    df_tiendas_ind = df_tiendas_ind.astype({'tienda': str})

    df_tiendas_ind['tienda'] = Util.capitalizarDatos(df_tiendas_ind['tienda'])

    df_tiendas_ind.loc[\
        df_tiendas_ind['tienda'].isin(['Todas', 'All']), 'all'] = True

    df_tiendas_ind['all'] = df_tiendas_ind['all'].fillna(False)

    df_tiendas_all = df_tiendas_ind.loc[df_tiendas_ind['all']]

    df_tiendas_ind = df_tiendas_ind.loc[~df_tiendas_ind['all']]

    df_tiendas_ind['tienda'] = \
        df_tiendas_ind['tienda'].str.replace('\\s+', ' ', regex=True)
    df_tiendas_ind = df_tiendas_ind.loc[df_tiendas_ind['tienda'].astype(bool)]

    return df_tiendas_all, df_tiendas_ind

def contarTiendas(data: pd.DataFrame) -> (pd.DataFrame):
    '''
        Verifica el número de tiendas que se piden asignar a un usuario
    '''

    df_tiendas_all = data[['email', 'id_cuenta']].loc[data['all'].eq(True)]
    df_tiendas_ind = data[['email', 'tienda']].loc[data['tienda'].notna() |
                                                    data['tienda'].ne('nan')]
    df_tiendas_state = \
        data[['email', 'id_cuenta', 'id_state']].loc[data['id_state'].notna()]

    df_tiendas_state = df_tiendas_state.loc[data['id_state'].ne('nan')]

    df_tiendas_state_all = \
        df_tiendas_state.loc[df_tiendas_state['id_cuenta'].isna() |
                                df_tiendas_state['id_cuenta'].eq('nan')]

    df_tiendas_state_account_all = \
        df_tiendas_state.loc[df_tiendas_state['id_cuenta'].notna() |
                                df_tiendas_state['id_cuenta'].ne('nan')]

    df_tiendas_state_account_all = \
        df_tiendas_state_account_all.\
            loc[df_tiendas_state_account_all['id_cuenta'].ne('nan')]

    # Todas las tiendas para una cuenta
    if len(df_tiendas_all) != 0:

        df_tiendas_all = df_tiendas_all.astype(str)
        cuentas = \
            Util.listarDatos(df_tiendas_all['id_cuenta'].drop_duplicates(),
                                fin=',\n')

        consultaAll = ('SELECT account_id, COUNT(*) FROM custos.places '
                        f'WHERE account_id IN (\n{cuentas}) AND status=1 '
                        'AND active_status=1 GROUP BY account_id;')

        Textos.agregaTexto(consultaAll)
        resultados = Db.ejecutarConsulta(consultaAll)

        df_resultados = pd.DataFrame(resultados,
                                        columns=['id_cuenta', 'num_tiendas'])

        df_resultados = df_resultados.astype({'id_cuenta':float})
        df_resultados = df_resultados.astype({'id_cuenta':str})

        df_tiendas_all = pd.merge(df_tiendas_all, df_resultados,
                                    on=['id_cuenta'], how='inner')
        df_tiendas_all.drop(columns=['id_cuenta'], inplace=True)

    # Todas las tiendas de un estado
    if len(df_tiendas_state_all) != 0:

        df_tiendas_state_all = df_tiendas_state_all.astype(str)
        estados = \
            Util.listarDatos(df_tiendas_state_all['id_state'].drop_duplicates(),
                                fin=',\n')

        consultaState = \
            ('SELECT b.id_state, COUNT(*) FROM custos.places a '
                'INNER JOIN custos.place_addresses b ON a.id=b.place_id '
                'INNER JOIN custos.accounts c ON c.id=a.account_id '
                f'WHERE b.id_state in ({estados}) AND a.status=1 '
                'AND a.active_status=1 AND c.status=1 '
                'AND c.active_status=1 AND c.indcustos=1 '
                'GROUP BY b.id_state;')

        Textos.agregaTexto(consultaState)
        resultados = Db.ejecutarConsulta(consultaState)

        df_resultados = pd.DataFrame(resultados,
                                        columns=['id_state', 'num_tiendas'])

        df_resultados = df_resultados.astype({'id_state': float})
        df_resultados = df_resultados.astype({'id_state': str})
        df_tiendas_state_all = df_tiendas_state_all.astype({'id_state': str})

        df_tiendas_state_all = pd.merge(df_tiendas_state_all, df_resultados,
                                        on=['id_state'], how='inner')
        df_tiendas_state_all.drop(columns=['id_state', 'id_cuenta'],
                                    inplace=True)

    # Todas las tiendas de un estado para una cuenta
    if len(df_tiendas_state_account_all) != 0:

        df_tiendas_state_account_all = df_tiendas_state_account_all.astype(str)
        estados = \
            Util.listarDatos(df_tiendas_state_account_all['id_state']\
                                .drop_duplicates(), fin=',\n')

        cuentas = \
            Util.listarDatos(df_tiendas_state_account_all['id_cuenta']\
                                .drop_duplicates(), fin=',\n')

        consultaStateAccount = \
            ('SELECT b.id_state, c.id, COUNT(*) FROM custos.places a '
                'INNER JOIN custos.place_addresses b ON a.id=b.place_id '
                'INNER JOIN custos.accounts c ON c.id=a.account_id '
                f'WHERE b.id_state IN ({estados}) '
                f'AND a.account_id IN ({cuentas}) AND a.status=1 '
                'AND a.active_status=1 AND c.status=1 '
                'AND c.active_status=1 AND c.indcustos=1 '
                'GROUP BY b.id_state, a.account_id;')

        Textos.agregaTexto(consultaStateAccount)

        resultados = Db.ejecutarConsulta(consultaStateAccount)

        df_resultados = \
            pd.DataFrame(resultados,
                            columns=['id_state', 'id_cuenta', 'num_tiendas'])

        df_resultados = \
            df_resultados.astype({'id_state':float, 'id_cuenta': float})
        df_resultados = df_resultados.astype({'id_state':str, 'id_cuenta': str})
        
        df_tiendas_state_account_all = \
            df_tiendas_state_account_all.astype({'id_state': str,
                                                    'id_cuenta': str})

        df_tiendas_state_account_all = \
            pd.merge(df_tiendas_state_account_all, df_resultados,
                        on=['id_state', 'id_cuenta'], how='inner')

        df_tiendas_state_account_all.drop(columns=['id_state', 'id_cuenta'],
                                            inplace=True)

    # Todas las tiendas
    if len(df_tiendas_ind) != 0:
        df_tiendas_num = df_tiendas_ind.groupby('email').count()

        df_tiendas_num.reset_index(inplace=True)
        df_tiendas_num.columns = ['email', 'num_tiendas']

    df_tiendas_total = \
        pd.concat([df_tiendas_num, df_tiendas_all,
                    df_tiendas_state_all, df_tiendas_state_account_all])

    df_tiendas_total = \
        df_tiendas_total.groupby(by=['email'], dropna=False).sum()

    df_tiendas_total = df_tiendas_total.reset_index()

    return df_tiendas_total

def buscarTiendas(data: pd.DataFrame) -> (pd.DataFrame):
    '''
        Busca el id de las tiendas
    '''

    df_tiendas_buscar = data[['id_cuenta', 'tienda', 'all']].drop_duplicates()

    df_tiendas_buscar = \
        df_tiendas_buscar.loc[df_tiendas_buscar['tienda'].notna()]

    if len(df_tiendas_buscar) == 0:
        data['id_tienda'] = np.nan
        return data

    df_tiendas_buscar = \
        df_tiendas_buscar.loc[df_tiendas_buscar['all'].eq(False)]

    df_tiendas_buscar.drop(columns=['all'], inplace=True)

    df_cuentas_tiendas = df_tiendas_buscar['id_cuenta'].drop_duplicates()

    df_tiendas_final = pd.DataFrame(columns=['id_cuenta', 'tienda',
                                                'id_tienda', 'store_number'])

    for _, cuenta in df_cuentas_tiendas.iteritems():

        df_tiendas_cuenta = buscarTiendasCuenta(df_tiendas_buscar, cuenta)

        df_tiendas_final = pd.concat([df_tiendas_final, df_tiendas_cuenta])

    df_tiendas_final.drop(columns=['store_number'], inplace=True)

    df_no_tiendas = df_tiendas_final.loc[df_tiendas_final['id_tienda'].isna()]

    if len(df_no_tiendas) != 0:

        df_no_tiendas.astype(str)
        df_no_tiendas['tienda_cuenta'] = \
            Util.concatValores(df_no_tiendas['tienda'],
                                df_no_tiendas['id_cuenta'], separador=', ')
        msg = 'No se encontraron las siguientes tiendas:\n'
        msg += Util.listarDatos(df_no_tiendas['tienda_cuenta'],
                                inicio='- ', fin='\n')
        print(msg)

        print('Desea buscarlas manualmente?\n'
                '1. Si\n'
                '2. No, en este caso se omitirá todo lo relacionado a estas')
        while True:
            op = input('> ')
            if op == '1':
                df_no_tiendas = df_no_tiendas.apply(buscarTiendasInd, axis=1)
                break
            elif op == '2':
                df_no_tiendas['id_tienda'] = np.nan
                break
            else:
                print('Selección inválida')

        df_no_tiendas.drop(columns=['tienda_cuenta'], inplace=True)
        df_tiendas_final = \
            df_tiendas_final.loc[df_tiendas_final['id_tienda'].notna()]

        df_tiendas_final = pd.concat([df_tiendas_final, df_no_tiendas])

    data = pd.merge(data, df_tiendas_final,
                            on=['tienda', 'id_cuenta'], how='left')

    return data

def buscarTiendasCuenta(df_tiendas: pd.DataFrame,
                        cuenta: int) -> (pd.DataFrame):
    '''
        Busca todas las tiendas para la cuenta indicada
    '''
    df_tiendas_cuenta = df_tiendas.loc[df_tiendas['id_cuenta'].eq(cuenta)]

    df_tiendas_cuenta = df_tiendas_cuenta.astype(str)

    df_tiendas_int = \
        df_tiendas_cuenta.loc[df_tiendas_cuenta['tienda'].str.isdigit()]
    df_tiendas_str = \
        df_tiendas_cuenta.loc[~df_tiendas_cuenta['tienda'].str.isdigit()]

    if len(df_tiendas_int) != 0:

        df_tiendas_int = buscarTiendasInt(df_tiendas_int, cuenta)

    if len(df_tiendas_str) != 0:

        df_tiendas_str = buscarTiendasStr(df_tiendas_str, cuenta)

    df_tiendas_cuenta = pd.concat([df_tiendas_int, df_tiendas_str])

    return df_tiendas_cuenta

def buscarTiendasInt(df_tiendas: pd.DataFrame, cuenta: int) -> (pd.DataFrame):
    '''
        Busca todas las tiendas numéricas para una cuenta en la base de datos
    '''

    datos = Util.listarDatos(df_tiendas['tienda'], fin=',\n')

    consulta = ('SELECT p.place_name, p.id, s.store_number '
                'FROM custos.places p INNER JOIN custos.store_details s '
                f'ON s.place_id=p.id WHERE (p.id IN (\n{datos}) '
                f'OR s.store_number IN (\n{datos}) '
                f'OR p.place_name IN (\n{datos})) '
                f'AND p.account_id={cuenta} '
                'AND p.status=1 AND p.active_status=1;')

    df_resultados = buscarTiendasGeneral(df_tiendas, consulta)

    if len(df_resultados) != 0:
        df_resultados = df_resultados.astype({'id_tienda': str,
                                                'store_number': str})

        df_resultados.loc[df_resultados['store_number']\
                            .isin(df_tiendas['tienda']),
                            'tienda'] = df_resultados['store_number']

        df_resultados.loc[df_resultados['id_tienda'].isin(df_tiendas['tienda']),
                            'tienda'] = df_resultados['id_tienda']

        df_tiendas = pd.merge(df_tiendas, df_resultados,
                                on=['tienda'], how='left')

    else:

        df_tiendas['id_tienda'] = np.nan
        df_tiendas['store_number'] = np.nan

    return df_tiendas

def buscarTiendasStr(df_tiendas: pd.DataFrame, cuenta: int) -> (pd.DataFrame):
    '''
        Busca todas las tiendas no numéricas para una cuenta en la base de datos
    '''
    datos = Util.listarDatos(df_tiendas['tienda'], inicio='\'', fin='\',\n')
    datos += '\''
    consulta = ('SELECT p.place_name, p.id, s.store_number '
                'FROM custos.places p INNER JOIN custos.store_details s '
                f'ON s.place_id=p.id WHERE p.place_name IN (\n{datos}) '
                f'AND p.account_id={cuenta} '
                'AND p.status=1 AND p.active_status=1;')

    df_resultados = buscarTiendasGeneral(df_tiendas, consulta)

    if len(df_resultados) != 0:
        df_resultados['tienda'] = Util.capitalizarDatos(df_resultados['tienda'])

        df_tiendas = pd.merge(df_tiendas, df_resultados,
                                on=['tienda'], how='left')

    else:

        df_tiendas['id_tienda'] = np.nan
        df_tiendas['store_number'] = np.nan

    return df_tiendas

def buscarTiendasGeneral(df_tiendas_cuenta: pd.DataFrame, consulta: str):
    '''
        Devuelve los resultados de las tiendas encontradas
    '''

    Textos.agregaTexto(consulta + '\n')
    resultados = Db.ejecutarConsulta(consulta)

    df_resultados = pd.DataFrame(resultados, columns=['tienda', 'id_tienda',
                                                        'store_number'])

    return df_resultados

def buscarTiendasInd(row: pd.Series) -> (pd.Series):
    '''
        Busca una tienda en el sistema y devuelve su id
    '''

    busqueda = str(row['tienda']).split()[0]

    consulta_busca_tiendas_manual = \
        ('SELECT p.place_name, p.id, s.store_number FROM custos.places p '
            'INNER JOIN custos.store_details s ON s.place_id=p.id '
            f'WHERE p.place_name like \'%{busqueda}%\' '
            f'AND p.account_id={row["id_cuenta"]} '
            'AND p.status=1 AND p.active_status=1;')

    row['id_tienda'] = selectConsultaUnica(consulta_busca_tiendas_manual,
                                            busqueda, str(row['id_cuenta']))

    return row

def selectConsultaUnica(consulta: str, busqueda: str, lugar: str) -> (int, int):
    '''
        Realiza una consulta individual para seleccionar la busqueda deseada
    '''

    Textos.agregaTexto(consulta)
    resultados = Db.ejecutarConsulta(consulta)

    if len(resultados) == 0:
        print(f'No se encontró {busqueda} en {lugar}')
        id_busqueda = np.nan

    elif len(resultados) == 1:
        print(f'Se encontró una única coincidencia para {busqueda} en {lugar}')
        id_busqueda = int(resultados[0][1])

    else:
        print(f'Para {busqueda}')
        for i, resul in enumerate(resultados, 1):
            print (f'{i} {resul}')

        while True:
            sel = input('> (s para omitir la busqueda) ')
            if sel == 's':
                id_busqueda = np.nan
                break
            elif not sel.isdigit():
                print('Selección inválida')
            else:
                sel = int(sel)
                sel -= 1
                if sel >= len(resultados) or sel < 0:
                    print('Selección inválida')
                else:
                    id_busqueda = int(resultados[sel][1])
                    break

    return id_busqueda

def setPhone(data: pd.DataFrame) -> (pd.DataFrame):
    '''
        Agrega los datos referentes al teléfono ()
    '''

    data_phone = data.loc[data['phone'].notna()]

    if len(data_phone) != 0:
        data_phone['phone'] = data_phone['phone'].replace(' ', '')
        data_phone['phone'] = data_phone['phone'].replace(r'\D', '')
        data_phone['phone'] = Util.ponerComillas(data_phone['phone'])

        data_phone['phone_type'], data_phone['phone_code'] = \
            Util.setPhoneData(data_phone['pais'])

        data_phone.astype(str)
        datos_phone = \
            Util.concatValores(data_phone['idaltop'], data_phone['phone_type'],
                                data_phone['phone'], data_phone['phone_code'],
                                '1', separador=', ')
        insertPhone = ('INSERT IGNORE INTO custos.person_contact (idaltop, '
                        'id_phone_type, phone_Number, phone_country_code_id, '
                        'status) VALUES\n')
    
        insertValue(insertPhone, datos_phone)
    
def ingresarPersona(data: pd.DataFrame):
    '''
        Ingresa las personas en el sistema
    '''

    dataUsers = (data[['id_tipo', 'id_rol', 'nombre1', 'nombre2', 'apellido1',
                        'apellido2', 'email', 'rol', 'pais']]
                        .drop_duplicates(subset=['email']))

    dataUsers.reset_index(inplace=True, drop=True)

    # Ejecutar el servicio para insertar la persona
    
    token = Servicio.getToken()
    
    dataUsers = dataUsers.apply(insertPerson, axis=1, token=token)
    
    dataUsers['nombre2'] = Util.ponerComillas(dataUsers['nombre2'])
    dataUsers['apellido2'] = Util.ponerComillas(dataUsers['apellido2'])
    
    dataUsersUpdate = dataUsers.loc[dataUsers['nombre2'].ne('null') |
                                    dataUsers['apellido2'].ne('null')]
    
    if len(dataUsersUpdate) != 0:
        for _, user in dataUsersUpdate.iterrows():
            
            update = 'UPDATE custos.person SET '
            if user['nombre2'] != 'null' and user['apellido2'] != 'null':
                update += (f'middle_name={user["nombre2"]}, '
                            f'second_last_name={user["apellido2"]} ')
            
            elif user['nombre2'] != 'null' and user['apellido2'] == 'null':
                update += f'middle_name={user["nombre2"]} ' 
                
            elif user['nombre2'] == 'null' and user['apellido2'] != 'null':
                update += f'second_last_name={user["apellido2"]} '
                
            update += f'WHERE idaltop={user["idaltop"]};'
                                 
            Textos.agregaTexto(update)
            Db.ejecutarCambio(update)
    
    data = pd.merge(data, dataUsers[['email', 'idaltop']],
                    on=['email'], how='inner')

    setPhone(data)

    dataAccounts = (data[['idaltop', 'id_cuenta']]
                    .loc[data['id_rol'].ne(2) & data['id_cuenta'].notna() &
                        ~(data.duplicated(subset=['idaltop', 'id_cuenta']))])

    dataAccounts = dataAccounts.loc[dataAccounts['id_cuenta'].ne('nan')]

    dataPlaces = data[['idaltop', 'id_tienda']]\
                    .loc[~(data['id_rol'].isin([2, 3]) | (data['id_rol'].eq(7) &
                        data['id_tipo'].isin([2, 14, 22, 18, 10, 35, 37]))) &
                        data['id_tienda'].notna() &
                        ~(data.duplicated(subset=['idaltop', 'id_tienda']))]

    dataPlacesAll = data[['idaltop', 'id_cuenta']].loc[data['all'] == True]
    dataPlacesAll = dataPlacesAll.drop_duplicates()

    dataStates = \
        data[['idaltop', 'id_cuenta', 'id_state']].loc[data['id_state'].notna()]

    dataPlacesDef = \
        data['idaltop'].loc[~(data['id_rol'].isin([2, 3]) |
                            (data['id_rol'].eq(7) &
                            data['id_tipo'].isin([2, 14, 22, 18, 10, 35, 37])))
                            & data['id_cuenta'].notna() &
                            ~(data.duplicated(subset=['idaltop']))]

    consulta_accounts = ('INSERT IGNORE INTO custos.people_accounts VALUES\n')
    consulta_places = ('INSERT IGNORE INTO custos.people_places VALUES\n')

    if len(dataAccounts) != 0:
        dataAccounts = dataAccounts.astype(str)

        datos_accounts = \
            Util.concatValores(dataAccounts['idaltop'],
                                dataAccounts['id_cuenta'], '1', CAMPOS_CONTROL,
                                separador=', ')

        insertValue(consulta_accounts, datos_accounts)

    if len(dataPlaces) != 0:
        dataPlaces = dataPlaces.astype(str)

        datos_places = \
            Util.concatValores(dataPlaces['id_tienda'], dataPlaces['idaltop'],
                                '1', CAMPOS_CONTROL, separador=', ')

        insertValue(consulta_places, datos_places)

    if len(dataPlacesAll) != 0:

        dataPlacesAll = dataPlacesAll.astype(str)

        df_places_all_cuentas = dataPlacesAll['id_cuenta'].drop_duplicates()

        df_places_all_cuentas.apply(insertPlacesAll, df_places=dataPlacesAll)

    if len(dataStates) != 0:

        dataStates = dataStates.astype(str)
        df_state_all = dataStates.loc[dataStates['id_cuenta'].isna() | \
            dataStates['id_cuenta'].eq('nan')]
        df_state_not_all = dataStates.loc[dataStates['id_cuenta'].\
                        notna() & dataStates['id_cuenta'].ne('nan')]

        df_state_all_states = df_state_all['id_state'].drop_duplicates()
        df_state_not_all_states = \
            df_state_not_all[['id_cuenta', 'id_state']].drop_duplicates()

        df_state_all_states.apply(insertStatesAll, df_states=df_state_all)

        df_state_not_all_states.apply(insertStatesPlaces,
                                        df_states=df_state_not_all, axis=1)

    if len(dataPlacesDef) != 0:

        dataPlacesDef = dataPlacesDef.astype(str)
        datos_places_def = Util.listarDatos(dataPlacesDef, fin=',\n')

        consulta_places_def = \
            ('INSERT IGNORE INTO custos.people_places '
                f'SELECT c.id, a.idaltop, 1, {CAMPOS_CONTROL} '
                'FROM custos.person a '
                'INNER JOIN custos.people_accounts b ON b.person_id=a.idaltop '
                'INNER JOIN custos.places c ON c.account_id=b.account_id '
                f'WHERE a.idaltop IN (\n{datos_places_def}) '
                'AND c.ind_default=1 AND c.status=1 AND c.active_status=1;')

        Textos.agregaTexto(consulta_places_def)
        Db.ejecutarCambio(consulta_places_def)

def insertPerson(row: pd.Series, token: str) -> (pd.Series):
    '''
        Inserta la persona desde el servicio
    '''

    if row['email'].find('walgreens') != -1:
        
        idaltop = \
            Servicio.addUser(token, row['email'], row['nombre1'],
                                row['apellido1'], row['pais'], row['id_tipo'],
                                row['id_rol'],'saml_auth')
    else:
        idaltop = \
            Servicio.addUser(token, row['email'], row['nombre1'],
                                row['apellido1'], row['pais'], row['id_tipo'],
                                row['id_rol'],'simple_auth')

    log_msg_user = (f'-- Se inserta el usuario {row["email"]} '
                    f'con id {idaltop}')
    Textos.agregaTexto(log_msg_user)
    row['idaltop'] = idaltop
    
    return row

def insertPlacesAll(cuenta: int, df_places: pd.DataFrame):
    '''
        Inserta todas las tiendas de una cuenta específica
    '''

    df_places_insert = df_places.loc[df_places['id_cuenta'].eq(cuenta)]

    idaltops = Util.listarDatos(df_places_insert['idaltop'], fin=',\n')

    consulta_places_all = \
        ('INSERT IGNORE INTO custos.people_places '
            f'SELECT b.id, a.idaltop, 1, {CAMPOS_CONTROL} '
            'FROM custos.person a, custos.places b '
            f'WHERE a.idaltop IN (\n{idaltops}) AND b.account_id={cuenta} '
            'AND b.status=1 AND b.active_status=1;')

    Textos.agregaTexto(consulta_places_all)
    Db.ejecutarCambio(consulta_places_all)

def insertStatesAll(state: int, df_states: pd.DataFrame):
    '''
        Inserta todas las cuentas y tiendas de un estado específico
    '''

    df_states_insert = df_states.loc[df_states['id_state'].eq(state)]
    idaltops = Util.listarDatos(df_states_insert['idaltop'], fin=',\n')

    consulta_state_accounts = \
        ('INSERT IGNORE INTO custos.people_accounts '
            f'SELECT DISTINCT a.idaltop, d.id, 1, {CAMPOS_CONTROL} '
            'FROM custos.person a, custos.places b '
            'INNER JOIN custos.place_addresses c ON c.place_id=b.id '
            'INNER JOIN custos.accounts d ON d.id=b.account_id '
            f'WHERE a.idaltop IN (\n{idaltops}) AND c.id_state={state} '
            'AND b.status=1 AND b.active_status=1 '
            'AND d.status=1 AND d.active_status=1 AND d.indcustos=1;')

    consulta_state_places = \
        ('INSERT IGNORE INTO custos.people_places '
            f'SELECT DISTINCT b.id, a.idaltop, 1, {CAMPOS_CONTROL} '
            'FROM custos.person a, custos.places b '
            'INNER JOIN custos.place_addresses c ON c.place_id=b.id '
            'INNER JOIN custos.accounts d ON d.id=b.account_id '
            f'WHERE a.idaltop IN (\n{idaltops}) AND c.id_state={state} '
            'AND b.status=1 AND b.active_status=1 '
            'AND d.status=1 AND d.active_status=1 AND d.indcustos=1;')

    Textos.agregaTexto(consulta_state_accounts)
    Textos.agregaTexto(consulta_state_places)

    Db.ejecutarCambio(consulta_state_accounts)
    Db.ejecutarCambio(consulta_state_places)

def insertStatesPlaces(row: pd.Series, df_states: pd.DataFrame):
    '''
        Inserta todas las tiendas de un estado para una cuenta específica
    '''

    cuenta = row['id_cuenta']
    state = row['id_state']
    df_states_insert = df_states.loc[df_states['id_cuenta'].eq(cuenta) &
                        df_states['id_state'].eq(state)]
    idaltops = Util.listarDatos(df_states_insert['idaltop'], fin=',\n')

    consulta_state_places = \
        ('INSERT IGNORE INTO custos.people_places '
            f'SELECT DISTINCT b.id, a.idaltop, 1, {CAMPOS_CONTROL} '
            'FROM custos.person a, custos.places b '
            'INNER JOIN custos.place_addresses c ON c.place_id=b.id '
            f'WHERE a.idaltop IN (\n{idaltops}) AND b.account_id={cuenta} '
            f'AND c.id_state={state} AND b.status=1 AND b.active_status=1;')

    Textos.agregaTexto(consulta_state_places)
    Db.ejecutarCambio(consulta_state_places)

def insertValue(consulta: str, datos: list):
    '''
        Inserta datos en la base de datos
    '''

    insert_datos = Util.listarDatos(datos, inicio='(', fin='),\n')
    insert_datos = insert_datos + ');'

    consulta += insert_datos

    Textos.agregaTexto(consulta)
    Db.ejecutarCambio(consulta)

def main():
    '''
        Principal que controla el flujo
    '''

    data = leerExcel()
    
    if len(data) == 0:
        print('Archivo vacío')
        return

    a = data['pais'].to_numpy() #Lo vuelve un array
    if not ((a[0] == a).all(0)):
        print('Error de países')
        print(a)
        return

    data_bad = data[data['nombre1'].isna() | data['apellido1'].isna()
                        | data['email'].isna() | data['rol'].isna()
                        | data['tipo'].isna()]
    if len(data_bad) != 0:
        print('Faltan datos obligatorios en: ')
        print(data_bad)
        return

    data['nombre1'], data['nombre2'], data['apellido1'], \
    data['apellido2'], data['email'] \
        = formatDatos(data['nombre1'], data['nombre2'], data['apellido1'],
                            data['apellido2'], data['email'])

    check_email = validarEmails(data)

    if check_email == 3:

        print('Usuarios existentes en el sistema o yopmail')
        return

    elif check_email == 2:

        print('Continuar?\n1. Si\n2. No')

        while True:
            op = input('> ')

            if op == '1':
                break
            elif op == '2':
                return
            else:
                print('Opción inválida')

    data['pais'] = Util.setPais(data['pais'])
    data['id_rol'] = Util.setRol(data['rol'])

    data.loc[data['id_rol'].isin([2, 3, 5]), 'tipo'] = 'Equipo Alto'
    data['id_tipo'] = Util.setTipo(data['pais'], data['tipo'])

    data = eliminarDuplicados(data)

    data_csr_bad = data.loc[data['csr_bad'] == True]
    if len(data_csr_bad) != 0:
        print('Client Store o Report sin cuenta asociada')
        print(data_csr_bad)
        return

    data.drop(columns=['csr_bad'], inplace=True)
    data = validarCuentas(data)

    df_no_cuentas = data[['email', 'cuenta']]\
        .loc[data['cuenta'].notna() & data['id_cuenta'].isna()]
        
    if len(df_no_cuentas) != 0:
        print('No se encontraron cuentas')
        print(df_no_cuentas)
        return

    data = validarEstados(data)

    df_no_states = data[['email', 'state']]\
        .loc[data['state'].notna() & data['id_state'].isna()]

    if len(df_no_states) != 0:
        print('No se encontraron estados')
        print(df_no_states)
        return

    data = formatTiendas(data)

    df_num_tiendas = contarTiendas(data)
    df_bad_num = df_num_tiendas.loc[df_num_tiendas['num_tiendas'].gt(200)]

    if len(df_bad_num) != 0:
        print('Para los siguientes usuarios se piden asignar '
                'más de 200 tiendas y esto puede ralentizar la App. '
                'Por favor confirmar la asignación de las tiendas:\n')
        print(df_bad_num)

        print('Continuar?\n1. Si\n2. No')
        while True:
            op = input('> ')

            if op == '1':
                break
            elif op == '2':
                return
            else:
                print('Opción inválida')
    
    data = buscarTiendas(data)


    df_no_tiendas = (data[['tienda', 'cuenta']]
        .loc[(data['tienda'].notna() | data['tienda'].eq('nan')) &
                (data['id_tienda'].isna() | data['id_tienda'].eq('nan')) &
                    data['all'].eq(False)])

    if len(df_no_tiendas) != 0:
        print('No es encontraron tiendas')
        print(df_no_tiendas)
        return

    ingresarPersona(data)

    Textos.crearArchivo()

if __name__ == '__main__':

    Db.iniciarConexion()

    main()

    Db.cerrarConexion()
