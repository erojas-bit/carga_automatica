from typing import Tuple
from numpy.testing import print_assert_equal
import validate_email
import pandas as pd
import numpy as np
import sys

sys.path.append('.')
from Util import Textos
from Util import Util
from Util import Db

def validarRepetido(df_emails: pd.DataFrame) -> (bool):
    '''
        Valida si los correos existen en el sistema
    '''

    correos = Util.listarDatos(df_emails['email'], inicio='\'', fin='\',\n')
    correos += '\''

    consulta_busca_emails = ('SELECT idaltop, email FROM custos.person '
                                f'WHERE email IN (\n{correos});')

    resultados = Db.ejecutarConsulta(consulta_busca_emails)

    rep_emails_msg = ('Los siguientes emails ya existen en el sistema, por '
                        'favor confirmar si se omiten o qué gestión tendrán:')
    
    if len(resultados) != 0:

        rep_emails_list = [i[0] for i in resultados]

        printEmailMsg(rep_emails_msg, rep_emails_list)

        return True

def validarExistencia(df_emails: pd.DataFrame,email:bool):
    '''
        Valida si los correos o ids existen en el sistema
    '''
    if len(df_emails) == 0: return
    if email:
        correos = Util.listarDatos(df_emails['email'], inicio='\'', fin='\',\n')
        correos += '\''

        consulta_busca_emails = ('SELECT idaltop as idaltop_abogado, email FROM custos.person '
                                    f'WHERE email IN (\n{correos}) and status = 1;')

        df_emails = pd.merge(df_emails,pd.read_sql(consulta_busca_emails,Db.db),
                              'left','email') 
        rep_emails_msg = ('Los siguientes emails no existen en el sistema:')
        resultados = df_emails.loc[pd.isna(df_emails['idaltop_abogado'])]
        if len(resultados) == 0:
            df_emails.rename(columns={"email":"abogado"},inplace=True)
            return df_emails[['idaltop_abogado','abogado']].drop_duplicates()
        elif len(resultados) != len(df_emails):
            rep_emails_list = [i for i in resultados['email']]

            printEmailMsg(rep_emails_msg, rep_emails_list)
            sys.exit()
            
            
            
    else:
        df_emails.drop_duplicates('abogado',inplace=True)
        correos = Util.listarDatos(df_emails['abogado'], inicio='\'', fin='\',\n')
        correos += '\''

        consulta_busca_emails = ('SELECT idaltop FROM custos.person '
                f'WHERE idaltop IN (\n{correos}) and '
            f'country_config_id = {df_emails["id_pais"].to_list()[0]};')

        resultados = Db.ejecutarConsulta(consulta_busca_emails)

        rep_emails_msg = ('Los siguientes idaltop no existen en el sistema:')

        if len(resultados) == 0:

            rep_emails_list = [i for i in df_emails['abogado']]

            printEmailMsg(rep_emails_msg, rep_emails_list)
            sys.exit()
        elif len(resultados) != len(df_emails):
            df_emails["check"] = df_emails["abogado"].\
                            isin([val[0] for val in resultados])
            df_no = df_emails.loc[~df_emails["check"]]
            if (len(df_no)> 0):
                rep_emails_list = [i for i in df_no['abogado']]

                printEmailMsg(rep_emails_msg, rep_emails_list)
                sys.exit() 



def validarFormato(df_emails: pd.DataFrame):
    '''
        Valida si los correos están en con formato adecuado
    '''

    checkEmailVector = np.vectorize(validate_email.validate_email)
    df_emails['check'] = checkEmailVector(df_emails['email'])

    bad_emails_list = df_emails['email'].loc[~df_emails['check']]
    
    bad_emails_msg = ('Los siguientes emails tienen un formato incorrecto, por '
                        'favor confirmar si se omiten o qué gestión tendrán:')

    if len(bad_emails_list) != 0:

        printEmailMsg(bad_emails_msg, bad_emails_list)
        sys.exit()

def validarPersonal(df_emails: pd.DataFrame) -> (bool):
    '''
        Valida si los correos son personales
    '''

    per_emails = ['gmail', 'hotmail', 'yahoo', 'outlook', 'ymail']

    per_emails_list = \
        df_emails['email'].loc[df_emails['dominio'].isin(per_emails)]

    per_emails_msg = ('Los siguientes emails son correos personales y podrían '
                        'recibir información sensible, por favor confirmar '
                        'si se omiten o qué gestión tendrán:')

    if len(per_emails_list) != 0:

        printEmailMsg(per_emails_msg, per_emails_list)

        return True

def validarYopmail(df_emails: pd.DataFrame) -> (bool):
    '''
        Valida si los correos son yopmail
    '''

    yop_emails_list = \
        df_emails['email'].loc[df_emails['dominio'].eq('yopmail')]

    yop_emails_msg = ('Los siguientes emails tienen dominio yopmail y no se '
                        'pueden crear, por favor confirmar si '
                        'se omiten o cuáles son los correctos:')

    if len(yop_emails_list) != 0:

        printEmailMsg(yop_emails_msg, yop_emails_list)

        return True


def user(emails: pd.Series) -> (bool):
    '''
        Hace 4 validaciones para los emails:
            1. Si existen en el sistema
            2. Si tienen formato correcto
            3. Si son correos personales
            4. Si son correos yopmail
    '''

    df_emails = pd.DataFrame(columns=['email', 'check', 'dominio'])

    df_emails['email'] = emails
    df_emails['dominio'] = Util.getDominio(df_emails['email'])

    rep = validarRepetido(df_emails)
    bad = validarFormato(df_emails)
    per = validarPersonal(df_emails)
    yop = validarYopmail(df_emails)

    if rep or yop:
        return 3
    elif bad or per:
        return 2
    else:
        return 1


def printEmailMsg(msg: str, datos: list) -> Tuple[str, str]:
    '''
        Enlista los datos al final del mensaje y lo imprime
    '''
    
    msg += Util.listarDatos(datos, inicio='\n- ')

    print(msg)