import re

texto = ""


def agregaTexto(nuevo: str):
    """
    Agrega el nuevo texto al texto total
    """

    global texto

    texto = texto + nuevo + "\n"


def crearArchivo(path:str):
    """
    Crea un archivo con un nombre ingresado por consola en la carpeta de casos
        con el texto recibido
    """

    global texto

    texto = re.sub("\\);\n", ");\n\n", texto)

    nombre = input("Ingrese el nombre del archivo: ")
    ruta = path + nombre + ".sql"
    archivo = open(ruta, "w")

    archivo.writelines(texto)
    archivo.close()
