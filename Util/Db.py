import mysql.connector

db = None
mycursor = None


def iniciarConexion():
    """
        Inicia la conexión con la base de Datos indicada:
            - Debe existir el archivo "credenciales" del ambiente a usar:
                port
                host
                user
                password

        Ambos archivos se deben agregar al .gitignore
    """

    ambiente = "PROD"    # PROD
    archivo = './Util/Credenciales/credenciales' + ambiente + '.txt'
    credenciales = open(archivo, 'r')

    datos = credenciales.readlines()
    host = datos[0].strip()
    port = datos[1].strip()
    user = datos[2].strip()
    pawd = datos[3].strip()

    global db
    db = mysql.connector.connect(
        host=host,
        port=port,
        user=user,
        passwd=pawd,
        database= 'custos'
    )

    global mycursor
    mycursor = db.cursor()


def ejecutarConsulta(consulta: str) -> (list):
    """
        Ejecuta una consulta SELECT en la Base de Datos
    """

    mycursor.execute(consulta)

    return mycursor.fetchall()


def ejecutarCambio(consulta: str):
    """
        Ejecuta una consulta que puede CAMBIAR la Base de Datos (ALTER, INSERT)
    """
    mycursor.execute(consulta)
    print(mycursor.rowcount)


def cerrarConexion():
    """
        Cierra la conexión con la Base de Datos
    """
    mycursor.close()
    db.commit()
    db.close()
