from Util import Db
from Util import Util
from Util import Textos
from Util import Servicio
import pandas as pd
import numpy as np
import sys

sys.path.append('.')


def Cuentas(data: pd.DataFrame) -> (pd.DataFrame):
    '''
        Crea una columna en el DataFrame inicial referente al id de la cuenta
    '''

    data.loc[data['cuenta'].notna(), 'cuenta'] = \
        Util.capitalizarDatos(data.loc[data['cuenta'].notna(), 'cuenta'])

    df_cuentas = data[['cuenta', 'pais']]
    df_cuentas = df_cuentas.loc[~(df_cuentas.duplicated() |
                                  df_cuentas['cuenta'].isna())]

    if len(df_cuentas) != 0:

        df_cuentas = buscarCuentas(df_cuentas)

        data = pd.merge(data, df_cuentas, on=['cuenta', 'pais'], how='left')

    else:
        data['id_cuenta'] = np.nan

    return data

def buscarCuentas(df_cuentas: pd.DataFrame) -> (pd.DataFrame):
    '''
        Obtiene el id de todas las cuentas enviadas, encontradas en el sistema
    '''

    df_cuentas.reset_index(drop=True, inplace=True)

    pais = df_cuentas['pais'][0]

    cuentas = Util.listarDatos(df_cuentas['cuenta'], inicio='\'', fin='\',\n')
    cuentas += '\''
    consulta_busca_cuentas = \
        (f'SELECT name, id FROM custos.accounts WHERE name IN (\n{cuentas}) '
            'AND status=1 AND active_status=1 AND indcustos=1 '
            f'AND country_config_id={pais};')

    Textos.agregaTexto(f'{consulta_busca_cuentas}\n')
    resultados = Db.ejecutarConsulta(consulta_busca_cuentas)

    if len(resultados) != 0:

        df_resultados = \
            pd.DataFrame(resultados, columns=['cuenta', 'id_cuenta'])
        df_resultados['cuenta'] = \
            Util.capitalizarDatos(df_resultados['cuenta'])

        df_cuentas = \
            pd.merge(df_cuentas, df_resultados, on=['cuenta'], how='left')

    else:
        df_cuentas['id_cuenta'] = np.nan

    df_no_cuentas = \
        df_cuentas[['cuenta', 'pais']].loc[df_cuentas['id_cuenta'].isna()]

    if len(df_no_cuentas) != 0:

        msg = 'No se encontraron las siguientes cuentas:\n'
        msg += Util.listarDatos(df_no_cuentas['cuenta'], inicio='- ', fin='\n')
        print(msg)

        print('Desea buscarlas manualmente?\n'
              '1. Si\n'
              '2. No, en este caso no se podrá continuar')

        while True:
            op = input('> ')
            if op == '1':
                df_no_cuentas = df_no_cuentas.apply(buscarCuentasInd, axis=1)
                break
            elif op == '2':
                df_no_cuentas['id_cuenta'] = np.nan
                break
            else:
                print('Selección inválida')

        df_cuentas = df_cuentas.loc[df_cuentas['id_cuenta'].notna()]

        df_cuentas = pd.concat([df_cuentas, df_no_cuentas])

    return df_cuentas

def buscarCuentasInd(row: pd.Series) -> (pd.Series):
    '''
        Busca una sola cuenta para seleccionarla manualmente
    '''

    busqueda = f'{row["cuenta"]}'.split()[0]

    consulta_busca_cuentas_manual = \
        ('SELECT name, id FROM custos.accounts '
            f'WHERE name like \'%{busqueda}%\''
            f'AND country_config_id={row["pais"]} AND status=1 '
            'AND active_status=1 AND indcustos=1;')

    row['id_cuenta'] = selectConsultaUnica(consulta_busca_cuentas_manual,
                                           busqueda, row['pais'])

    return row


def validarEstados(data: pd.DataFrame) -> (pd.DataFrame):
    '''
        Crea una columna en el DataFrame inicial referente al id del estado
    '''

    df_states = \
        data[['email', 'state', 'id_cuenta', 'pais']].loc[data['state'].notna()]

    if len(df_states) != 0:

        df_states = ajustarEstados(df_states)

        df_states = buscarEstados(df_states)

        data.drop(columns=['state'], inplace=True)
        data = data.astype({'id_cuenta': str})

        data = pd.merge(data, df_states,
                        on=['email', 'id_cuenta', 'pais'], how='left')

    else:
        data['id_state'] = np.nan

    return data

def ajustarEstados(df_states: pd.DataFrame) -> (pd.DataFrame):
    '''
        Ajusta cada estado en filas diferentes
    '''

    # Separa los estados por un separador (~) pero las mantiene atadas tanto a
    # la cuenta como al usuario correspondiente

    df_states['concatenacion'] = \
        Util.concatValores(df_states['email'],
                           df_states['id_cuenta'].astype(str), separador='~')

    df_states_ind = pd.DataFrame(df_states['state'].str.split('~').tolist(),
                                 index=df_states['concatenacion']).stack()

    df_states.reset_index(drop=True, inplace=True)
    df_states_ind = df_states_ind.reset_index([0, 'concatenacion'])

    df_states_ind[['email', 'id_cuenta']] = \
        df_states_ind['concatenacion'].str.split('~', expand=True)

    df_states_ind.drop(columns=['concatenacion'], inplace=True)

    df_states_ind.columns = ['state', 'email', 'id_cuenta']

    df_states_ind['state'] = Util.capitalizarDatos(df_states_ind['state'])

    df_states_ind['state'] = \
        df_states_ind['state'].str.replace('\\s+', ' ', regex=True)

    df_states_ind['pais'] = df_states['pais'][0]

    return df_states_ind

def buscarEstados(df_states: pd.DataFrame) -> (pd.DataFrame):
    '''
        Obtiene el id de todos los estados encontradas en el sistema
    '''

    pais = df_states['pais'][0]

    states = Util.listarDatos(df_states['state'].drop_duplicates(),
                              inicio='\'', fin='\',\n')
    states += '\''
    consulta_busca_state = ('SELECT state, id_state FROM custos.state '
                            f'WHERE state IN (\n{states}) '
                            f'AND id_country={pais};')

    Textos.agregaTexto(consulta_busca_state + '\n')
    resultados = Db.ejecutarConsulta(consulta_busca_state)

    if len(resultados) != 0:

        df_resultados = pd.DataFrame(resultados,
                                     columns=['state', 'id_state'])
        df_resultados['state'] = Util.capitalizarDatos(df_resultados['state'])

        df_states = pd.merge(df_states, df_resultados,
                             on=['state'], how='left')

    else:
        df_states['id_state'] = np.nan

    df_no_state_all = (df_states[['email', 'id_cuenta', 'state', 'pais']]
                       .loc[df_states['id_state'].isna()])

    df_no_states = df_no_state_all[['state', 'pais']].drop_duplicates()

    if len(df_no_states) != 0:

        msg = 'No se encontraron los siguientes estados:\n'
        msg += Util.listarDatos(df_no_states['state'], inicio='- ', fin='\n')
        print(msg)

        print('Desea buscarlos manualmente?\n'
              '1. Si\n'
              '2. No, en este caso no se podrá continuar')
        while True:
            op = input('> ')
            if op == '1':

                df_no_states = df_no_states.apply(buscarEstadosInd, axis=1)
                df_no_state_all = pd.merge(df_no_state_all, df_no_states,
                                           on=['state', 'pais'], how='left')
                break
            elif op == '2':
                df_no_state_all['id_state'] = np.nan
                break
            else:
                print('Selección inválida')

        df_states = df_states.loc[df_states['id_state'].notna()]

        df_states = pd.concat([df_states, df_no_state_all])

    return df_states


def buscarEstadosInd(row: pd.Series) -> (pd.Series):
    '''
        Busca un solo estado para seleccionarlo manualmente
    '''

    busqueda = str(row['state']).split()[0]

    consulta_busca_states_manual = ('SELECT state, id_state FROM custos.state '
                                    f'WHERE state like \'%{busqueda}%\' '
                                    f'AND id_country={row["pais"]};')

    row['id_state'] = selectConsultaUnica(consulta_busca_states_manual,
                                          busqueda, str(row['pais']))

    return row


def buscarTiendas(data: pd.DataFrame) -> (pd.DataFrame):
    '''
        Busca el id de las tiendas
    '''

    df_tiendas_buscar = data[['id_cuenta', 'tienda', 'all']].drop_duplicates()

    df_tiendas_buscar = \
        df_tiendas_buscar.loc[df_tiendas_buscar['tienda'].notna()]

    if len(df_tiendas_buscar) == 0:
        data['id_tienda'] = np.nan
        return data

    df_tiendas_buscar = \
        df_tiendas_buscar.loc[df_tiendas_buscar['all'].eq(False)]

    df_tiendas_buscar.drop(columns=['all'], inplace=True)

    df_cuentas_tiendas = df_tiendas_buscar['id_cuenta'].drop_duplicates()

    df_tiendas_final = pd.DataFrame(columns=['id_cuenta', 'tienda',
                                             'id_tienda', 'store_number'])

    for _, cuenta in df_cuentas_tiendas.iteritems():

        df_tiendas_cuenta = buscarTiendasCuenta(df_tiendas_buscar, cuenta)

        df_tiendas_final = pd.concat([df_tiendas_final, df_tiendas_cuenta])

    df_tiendas_final.drop(columns=['store_number'], inplace=True)

    df_no_tiendas = df_tiendas_final.loc[df_tiendas_final['id_tienda'].isna()]

    if len(df_no_tiendas) != 0:

        df_no_tiendas.astype(str)
        df_no_tiendas['tienda_cuenta'] = \
            Util.concatValores(df_no_tiendas['tienda'],
                               df_no_tiendas['id_cuenta'], separador=', ')
        msg = 'No se encontraron las siguientes tiendas:\n'
        msg += Util.listarDatos(df_no_tiendas['tienda_cuenta'],
                                inicio='- ', fin='\n')
        print(msg)

        print('Desea buscarlas manualmente?\n'
              '1. Si\n'
              '2. No, en este caso no se podrá continuar')
        while True:
            op = input('> ')
            if op == '1':
                df_no_tiendas = df_no_tiendas.apply(buscarTiendasInd, axis=1)
                break
            elif op == '2':
                df_no_tiendas['id_tienda'] = np.nan
                break
            else:
                print('Selección inválida')

        df_no_tiendas.drop(columns=['tienda_cuenta'], inplace=True)
        df_tiendas_final = \
            df_tiendas_final.loc[df_tiendas_final['id_tienda'].notna()]

        df_tiendas_final = pd.concat([df_tiendas_final, df_no_tiendas])

    data = pd.merge(data, df_tiendas_final,
                    on=['tienda', 'id_cuenta'], how='left')

    return data


def buscarTiendasCuenta(df_tiendas: pd.DataFrame,
                        cuenta: int) -> (pd.DataFrame):
    '''
        Busca todas las tiendas para la cuenta indicada
    '''
    df_tiendas_cuenta = df_tiendas.loc[df_tiendas['id_cuenta'].eq(cuenta)]

    df_tiendas_cuenta = df_tiendas_cuenta.astype(str)

    df_tiendas_int = \
        df_tiendas_cuenta.loc[df_tiendas_cuenta['tienda'].str.isdigit()]
    df_tiendas_str = \
        df_tiendas_cuenta.loc[~df_tiendas_cuenta['tienda'].str.isdigit()]

    if len(df_tiendas_int) != 0:

        df_tiendas_int = buscarTiendasInt(df_tiendas_int, cuenta)

    if len(df_tiendas_str) != 0:

        df_tiendas_str = buscarTiendasStr(df_tiendas_str, cuenta)

    df_tiendas_cuenta = pd.concat([df_tiendas_int, df_tiendas_str])

    return df_tiendas_cuenta


def buscarTiendasInt(df_tiendas: pd.DataFrame, cuenta: int) -> (pd.DataFrame):
    '''
        Busca todas las tiendas numéricas para una cuenta en la base de datos
    '''

    datos = Util.listarDatos(df_tiendas['tienda'], fin=',\n')

    consulta = ('SELECT p.place_name, p.id, s.store_number '
                'FROM custos.places p INNER JOIN custos.store_details s '
                f'ON s.place_id=p.id WHERE (p.id IN (\n{datos}) '
                f'OR s.store_number IN (\n{datos}) '
                f'OR p.place_name IN (\n{datos})) '
                f'AND p.account_id={cuenta} '
                'AND p.status=1 AND p.active_status=1;')

    df_resultados = buscarTiendasGeneral(df_tiendas, consulta)

    if len(df_resultados) != 0:
        df_resultados = df_resultados.astype({'id_tienda': str,
                                              'store_number': str})

        df_resultados.loc[df_resultados['store_number']
                          .isin(df_tiendas['tienda']),
                          'tienda'] = df_resultados['store_number']

        df_resultados.loc[df_resultados['id_tienda'].isin(df_tiendas['tienda']),
                          'tienda'] = df_resultados['id_tienda']

        df_tiendas = pd.merge(df_tiendas, df_resultados,
                              on=['tienda'], how='left')

    else:

        df_tiendas['id_tienda'] = np.nan
        df_tiendas['store_number'] = np.nan

    return df_tiendas


def buscarTiendasStr(df_tiendas: pd.DataFrame, cuenta: int) -> (pd.DataFrame):
    '''
        Busca todas las tiendas no numéricas para una cuenta en la base de datos
    '''
    datos = Util.listarDatos(df_tiendas['tienda'], inicio='\'', fin='\',\n')
    datos += '\''
    consulta = ('SELECT p.place_name, p.id, s.store_number '
                'FROM custos.places p INNER JOIN custos.store_details s '
                f'ON s.place_id=p.id WHERE p.place_name IN (\n{datos}) '
                f'AND p.account_id={cuenta} '
                'AND p.status=1 AND p.active_status=1;')

    df_resultados = buscarTiendasGeneral(df_tiendas, consulta)

    if len(df_resultados) != 0:
        df_resultados['tienda'] = Util.capitalizarDatos(
            df_resultados['tienda'])

        df_tiendas = pd.merge(df_tiendas, df_resultados,
                              on=['tienda'], how='left')

    else:

        df_tiendas['id_tienda'] = np.nan
        df_tiendas['store_number'] = np.nan

    return df_tiendas


def buscarTiendasGeneral(df_tiendas_cuenta: pd.DataFrame, consulta: str):
    '''
        Devuelve los resultados de las tiendas encontradas
    '''

    Textos.agregaTexto(consulta + '\n')
    resultados = Db.ejecutarConsulta(consulta)

    df_resultados = pd.DataFrame(resultados, columns=['tienda', 'id_tienda',
                                                      'store_number'])

    return df_resultados


def buscarTiendasInd(row: pd.Series) -> (pd.Series):
    '''
        Busca una tienda en el sistema y devuelve su id
    '''

    busqueda = str(row['tienda']).split()[0]

    consulta_busca_tiendas_manual = \
        ('SELECT p.place_name, p.id, s.store_number FROM custos.places p '
            'INNER JOIN custos.store_details s ON s.place_id=p.id '
            f'WHERE p.place_name like \'%{busqueda}%\' '
            f'AND p.account_id={row["id_cuenta"]} '
            'AND p.status=1 AND p.active_status=1;')

    row['id_tienda'] = selectConsultaUnica(consulta_busca_tiendas_manual,
                                           busqueda, str(row['id_cuenta']))

    return row


def selectConsultaUnica(consulta: str, busqueda: str, lugar: str) -> (int, int):
    '''
        Realiza una consulta individual para seleccionar la busqueda deseada
    '''

    Textos.agregaTexto(consulta)
    resultados = Db.ejecutarConsulta(consulta)

    if len(resultados) == 0:
        print(f'No se encontró {busqueda} en {lugar}')
        id_busqueda = np.nan

    elif len(resultados) == 1:
        print(f'Se encontró una única coincidencia para {busqueda} en {lugar}')
        id_busqueda = resultados[0][1]

    else:
        print(f'Para {busqueda}')
        for i, resul in enumerate(resultados, 1):
            print(f'{i} {resul}')

        while True:
            sel = input('> (s para omitir la busqueda) ')
            if sel == 's':
                id_busqueda = np.nan
                break
            elif not sel.isdigit():
                print('Selección inválida')
            else:
                sel = int(sel)
                sel -= 1
                if sel >= len(resultados) or sel < 0:
                    print('Selección inválida')
                else:
                    id_busqueda = int(resultados[sel][1])
                    break

    return id_busqueda
