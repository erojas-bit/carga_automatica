import numpy as np
import re
from unicodedata import normalize

@np.vectorize
def setPais(pais: str) -> (int):
    """
        Devuelve los datos del pais respectivo
    """

    switchPais = {
        "Usa": 1,
        "Chile": 2,
        "Colombia": 3,
        "México": 4,
        "España": 5,
        "Demo US": 6,
        "Demo ES": 7
    }

    return switchPais.get(pais)

@np.vectorize
def tipo_servicio_tienda(pais:int,tipo:str):
    if tipo == "NULL": return "NULL"
    tipo_servicio_id = {
        4:{"A":"5747",
        "AA":"5754",
        "C":"7169"}
    }
    if pais not in tipo_servicio_id: 
        print(f"{pais} no tiene tipo de servicio tienda")
        return "NULL"
        
    if tipo not in tipo_servicio_id[pais]:
        print(f"el tipo de servicio {tipo} no está definido en {pais}, por favor verificar")
        exit()
    
    return tipo_servicio_id[pais][tipo.upper()]

@np.vectorize
def setRol(rol: str) -> (int):
    """
        Devuelve los datos del rol respectivo
    """

    rol = rol.title()
    switchId = {
        "Sysadmin": 2,
        "Kam": 3,
        "Lawyer": 5,
        "Client Corporate": 7,
        "Client Store": 9,
        "Client Report": 20,
        "Call Center": 23
    }

    id_ = switchId.get(rol)
    return id_

@np.vectorize
def setTipo(pais: int, tipo: str) -> (int):
    """
        Devuelve los datos del tipo respectivo
    """

    switchAlto = {
        1: 2,
        2: 14,
        3: 22,
        4: 18,
        5: 10,
        6: 35,
        7: 37
    }

    switchCliente = {
        1: 3,
        2: 15,
        3: 23,
        4: 19,
        5: 11,
        6: 36,
        7: 38
    }

    tipo = tipo.title()
    if tipo == "Equipo Alto" or tipo == "Alto Team":
        return (switchAlto.get(pais))
    elif tipo == "Cliente" or tipo == "Client":
        return (switchCliente.get(pais))

@np.vectorize
def setPhoneData(pais: int) -> (int):
    """
        Devuelve los datos del tipo respectivo
    """

    switchType = {
        1: 1,
        2: 33,
        3: 40,
        4: 36,
        5: 22,
        6: 48,
        7: 57
    }

    switchCode = {
        1: 1,
        2: 3,
        3: 2,
        4: 4,
        5: 5,
        6: 6,
        7: 7,
    }

    return (switchType.get(pais), switchCode.get(pais))

@np.vectorize
def getDominio(email: str) -> (str):
    """
        Obtiene las partes de un email
    """

    arroba = email.rfind("@")
    fin = email.find(".", arroba)

    dominio = email[arroba + 1:fin]

    return dominio

@np.vectorize
def ponerComillas(dato: str) -> (str):

    if dato != "null":

        if "'" in dato:
            dato = f'"{dato}"'
        else:
            dato = f"'{dato}'"

    return dato

@np.vectorize
def concatValores(*args, separador: str):

    return separador.join(args)

@np.vectorize
def capitalizarDatos(dato: str) -> (str):
    """
        Devuelve un dato de entrada capitalizado y sin espacios a los lados
    """
    return f'{dato}'.title().strip()

@np.vectorize
def phone_type(id_country):

    phone_type_id = {
        1: 1,
        2:33,
        3:40,
        4:37,
        5:22,
        6:48,
        7:57
    }
    return phone_type_id[id_country]

@np.vectorize
def phone_country_code(id_country):

    phone_country_code_id = {
        1: 1,
        2:3,
        3:2,
        4:4,
        5:5,
        6:6,
        7:7
    }
    return phone_country_code_id[id_country]


def listarDatos(lista: list, inicio: str="", fin: str="") -> (str):
    """
        Devuelve los datos de una lista en forma de string listado
    """

    return (f'{inicio}{f"{fin}{inicio}".join(lista)}')

def normalizar(value:str)->str:
    s = re.sub(
        r"([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+", r"\1", 
        normalize( "NFD", value), 0, re.I)
    # -> NFC
    s = normalize( 'NFC', s)

    return s.replace("ñ","n").strip().lower().replace(" ","")