import requests
import sys

#URL = 'https://www.altoalliance.com/api/v2'
URL = 'https://custos-developer.altoalliance.com/api/v2'

def getToken():
    """
        Se autentica en el servicio
    """
    
    url = URL + "/authenticate"
    body = {
        "email": "internal_systemMF@mail.com",
        "password": "Alto2020"
    }
    
    response = requests.post(url, json=body)
    
    if response.status_code == 200:
        
        return response.json().get('auth').get('token')
    
    else:
        
        print("Falló el log")
        sys.exit()
        return ""

def addUser(token: str, email: str, first_name: str, last_name: str,
            id_pais: int, id_tipo: int, id_rol: int, auth_method: str, index):
    language = "es"
    if id_pais in (1, 6):
        language = "en"
    body = {
        "email": email,
        "first_name": first_name,
        "last_name": last_name,
        "country_id": id_pais,
        "person_type_id": id_tipo,
        "idalto_rol": id_rol,
        "auth_method": auth_method,
        "language": language,
        "encrypted_password": "false"
    }
    response = requests.post(url=URL + "/users", json=body,
                                headers={'Authorization': token})

    if  response.ok:
        print(f'Usuario {index}. {email} con método de autenticación {body.get("auth_method")}')
        return response.json().get("id")
    
    print("Ocurrió un error ")
    print(response.text)
    sys.exit()