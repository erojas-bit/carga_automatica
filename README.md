Se debe ejecutar el archivo Src/Usuario.py

Se deben instalar las siguientes librerías en caso de no tenerlas:
- pip3 install unidecode
- pip3 install validate_email
- pip3 install tkinter
- pip3 install pandas
- pip3 install numpy
- pip3 install mysql-connector
- pip3 install xlrd
- pip3 install openpyxl

Se debe crear la carpeta Util/Credenciales con dos archivos:
- credenciales DEV.txt
- credenciales PROD.txt
Cada uno con los datos de conexión en el siguiente orden:
- host
- port
- user
- password
En el archivo Util/Servicio.py se debe cambiar el body en la función get_token()